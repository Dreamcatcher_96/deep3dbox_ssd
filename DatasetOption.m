% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期五 30/12/2016 10:11.
% Last Revision: 星期五 30/12/2016 10:11.
%qt

function data_opt = DatasetOption(dataset_name)

    switch dataset_name
    case 'voc0712'
        data_opt.R = 0.8; % train v.s. test
        data_opt.R_val = 0; % train v.s. validation
        data_opt.data_root_path = '~/work/VOCdevkit/VOC0712';
        data_opt.annotation_file = fullfile(data_opt.data_root_path, 'data_list.mat');
        data_opt.image_root = fullfile(data_opt.data_root_path, 'images');
        data_opt.train_test_list_file = fullfile(data_opt.data_root_path, ...
                        sprintf('train_test_img_list_R_%.1f.mat', data_opt.R));
    otherwise
    end

    data_opt.max_expand_ratio = 4;
    data_opt.min_expand_ratio = 1.1;
    data_opt.distort_prob = 0.5;
    data_opt.flip_prob = 0.5;
    data_opt.expand_prob = 0.5;

end
