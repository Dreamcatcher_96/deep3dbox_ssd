% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:07.
% Last Revision: 星期二 24/01/2017 23:07.
%qt

function [pos_dbox_all, pos_gbox_all, neg_dbox_all, num_pos, num_neg] = ...
                SSDSelectPosNegSample(conf_pred, dbox, net_input_label, net_opt)

    pos_dbox_all = cell(1, net_opt.batch_size);
    pos_gbox_all = cell(1, net_opt.batch_size);
    neg_dbox_all = cell(1, net_opt.batch_size);
    num_pos = 0;
    num_neg = 0;
    for i_im = 1 : net_opt.batch_size

        % the IoU between the default boxes and the ground-truth.
        overlap_mat = CalcBoxOverlap(dbox, net_input_label{i_im}(:, 1 : 4)', 'int_uni'); % 找到每个priorbox和ground_truth的IoU
        [~, dbox_idx] = max(overlap_mat, [], 1);
        overlap_mat(sub2ind(size(overlap_mat), dbox_idx, 1:size(overlap_mat, 2))) = inf;
        [max_dbox_overlap, gbox_idx] = max(overlap_mat, [], 2);

        % the prediction and the ground-truth of the positive samples.
        pos_dbox_idx = find(max_dbox_overlap >= net_opt.pos_overlap);       % 找到正样本的索引
        matched_gt_idx = gbox_idx(pos_dbox_idx);
        num_pos = num_pos + numel(matched_gt_idx);

        % hard negative mining
        if isempty(overlap_mat)
            neg_idx = 1:size(conf_pred, 2);
        else
            neg_idx = find(max_dbox_overlap < net_opt.neg_overlap);
        end
        neg_score = SSDNegConfLoss(conf_pred(:, :, i_im), neg_idx);
        [~, sort_neg_idx] = sort(neg_score, 'descend');

        % pick top num_neg negatives
        if isempty(overlap_mat)
            pick_neg_num = numel(neg_idx) / 3;
        else
            pick_neg_num = min(numel(neg_idx), ...
                    net_opt.neg_pos_ratio * numel(pos_dbox_idx)); 
        end
        neg_idx = neg_idx(sort_neg_idx(1 : pick_neg_num));
        num_neg = num_neg + pick_neg_num;

        pos_dbox_all{i_im} = pos_dbox_idx;
        pos_gbox_all{i_im} = matched_gt_idx;
        neg_dbox_all{i_im} = neg_idx;

    end


end



