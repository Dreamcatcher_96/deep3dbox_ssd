% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期四 23/02/2017 11:02.
% Last Revision: 星期四 23/02/2017 11:02.

function ShowDetectionResults(hf, im, bbox_pred, ...
    score_pred, all_class_name, all_class_color)

    figure(hf);
    show_score_thre = 0.5;

    num_class = size(bbox_pred);
    for i_class = 1 : num_class

        large_idx = score_pred{i_class} > show_score_thre;
        bbox_pred_now = bbox_pred{i_class}(:, large_idx);
        score_pred_now = score_pred{i_class}(large_idx);
        n_det = size(bbox_pred_now, 2);
        
        if n_det == 0, continue; end
        
        show_label = cell(n_det, 1);
        show_color = zeros(n_det, 3);
        for i_det = 1 : n_det
            show_label{i_det} = ['Pred-', all_class_name{i_class}, '-', ...
                                        sprintf('%2.2f', score_pred_now(i_det))];
            show_color(i_det, :) = all_class_color(i_class, :);
        end
        show_bbox = (LTRB2LTWH(bbox_pred_now))';
        im = insertObjectAnnotation(uint8(im), 'rectangle', show_bbox, ...
                    show_label, 'TextBoxOpacity', 0.9, 'FontSize', 18, ...
                                                    'Color', show_color, 'LineWidth', 3);
    end

    imagesc(im);axis equal;

end


%     num_img = numel(img_cell);
%     n_row = floor(sqrt(num_img));
%     n_col = ceil(num_img / n_row);
%     for i_row = 1 : n_row
%         for i_col = 1 : n_col
%     k = (i_row - 1) * n_col + i_col;
%     if k > num_img, break; end
%         end
%     end

%     n_gt = size(img_obj_bbox, 2);
%     show_label = cell(n_gt, 1);
%     show_color = zeros(n_gt, 3);
%     gt_name = all_class_name(img_obj_class);
%     for i_gt = 1 : n_gt
%         show_label{i_gt} = ['GT-', gt_name{i_gt}];
%         show_color(i_gt, :) = all_class_color(img_obj_class(i_gt), :);
%     end
%     show_bbox = (LTRB2LTWH(img_obj_bbox))';
%     im = insertObjectAnnotation(im, 'rectangle', show_bbox, ...
%                 show_label, 'TextBoxOpacity', 0.9, 'FontSize', 18, ...
%                                                 'Color', show_color);

