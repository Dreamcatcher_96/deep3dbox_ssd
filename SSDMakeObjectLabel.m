% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期三 11/01/2017 17:39.
% Last Revision: 星期三 11/01/2017 17:39.
%qt

function ssd_label = SSDMakeObjectLabel(obj_bbox, obj_class, obj_conf, obj_loc, sub_img)

    img_siz = cellfun(@(x) [size(x, 2); size(x, 1); ...
        size(x, 2); size(x, 1)], sub_img, 'UniformOutput', false);
    obj_bbox = cellfun(@(x, y) bsxfun(@rdivide, x, y), ...
                        obj_bbox, img_siz, 'UniformOutput', false);

    ssd_label = cellfun(@(x, y, z, w) cat(2, x', y', z', w'), ...
        obj_bbox, obj_class, obj_conf, obj_loc, 'UniformOutput', false);

end



