function [ loc_loss_struct ] = AngleL2Forward( loc_pred_data, loc_gt_data)

    loc_loss_struct.diff    = zeros(size(loc_pred_data));
    loc_loss_struct.errors  = zeros(size(loc_pred_data));
%     loc_pred_data = max(-1, min(1, loc_pred_data));
    loss = (loc_gt_data - loc_pred_data) .^ 2; % + (loc_gt_data(:, 1) - loc_pred_data(:, 1)) .^ 2;
%     eps = 1 * 10 ^ -6;
    loc_loss_struct.diff = 2 * (loc_pred_data - loc_gt_data);
%     angle_gt = DecodePose((conf_gt_data+1)', loc_gt_data', net_opt) * pi / 180;
%     angle_pred = DecodePose((conf_gt_data+1)', loc_pred_data', net_opt) * pi / 180;

%     angle_gt = atan2(loc_gt_data(:, 1), loc_gt_data(:, 2));
%     angle_pred = atan2(loc_pred_data(:, 1), loc_pred_data(:, 2));
%     
%     loss = cos(angle_gt - angle_pred);
%     loc_loss_struct.diff = angle_gt - angle_pred;

%     loss = loc_gt_data(:, 2) .* loc_pred_data(:, 2) + loc_gt_data(:, 1) .* loc_pred_data(:, 1);
% 
%     loc_loss_struct.diff(:, 1) = loc_gt_data(:, 1);
%     loc_loss_struct.diff(:, 2) = loc_gt_data(:, 2);
                        
%     loc_loss_struct.loc_pred_diff = [sin(loc_loss_struct.diff), sin(loc_loss_struct.diff)];
%     loc_loss_struct.loc_pred_diff = loc_loss_struct.loc_pred_diff .* ...
%                                 [loc_pred_data(:, 2), -loc_pred_data(:, 1)] ./ [sum(loc_pred_data.^2, 2), sum(loc_pred_data.^2, 2)];
    
%     loss = (loc_gt_data(:, 2) - loc_pred_data(:, 2)) .^ 2 + (loc_gt_data(:, 1) - loc_pred_data(:, 1)) .^ 2;
%     
%     loc_loss_struct.diff = -2 * (loc_gt_data - loc_pred_data) .* loc_pred_data;
    
    loc_loss_struct.loss    = sum(loss(:));
end