function GeneratePriorbox()
    img_size = [224, 224];
    layer_width = [28, 14];
    min_size = [74, 149];
    max_size = [149, 224];
    clip = true;
    step_size = [img_size(1)/layer_width(1), img_size(1)/layer_width(2)];
%     step_size = [8, 16, 32, 64, 100, 300];
    ar = [1, 1, 2, 1/2, 3, 1/3];
%     ar_ = [1, 1, 2, 1/2];
    priorbox = zeros(sum(layer_width.^2)*6, 4);
    offset = 0.5;
    cnt = 1;
    for i = 1:numel(layer_width)
        for h = 0:layer_width(i)-1
            for w = 0:layer_width(i)-1
                center_x = (w + offset) * step_size(i);
                center_y = (h + offset) * step_size(i);
                for j = 1:6
                    if j ~= 2
                        box_width = min_size(i)*sqrt(ar(j));                
                        box_height = min_size(i)/sqrt(ar(j));
                    else
                        box_width = sqrt(min_size(i)*max_size(i))*sqrt(ar(j));                
                        box_height = sqrt(min_size(i)*max_size(i))/sqrt(ar(j));
                    end
                    priorbox(cnt, 1) = (center_x - box_width / 2) / img_size(1);
                    priorbox(cnt, 2) = (center_y - box_height / 2) / img_size(2);
                    priorbox(cnt, 3) = (center_x + box_width / 2) / img_size(1);
                    priorbox(cnt, 4) = (center_y + box_height / 2) / img_size(2);
                    cnt = cnt + 1;
                end
            end
        end
    end
    if clip
        priorbox(priorbox<0) = 0;
        priorbox(priorbox>1) = 1;
    end
    save('priorbox_3scale_2branch.mat', 'priorbox');
end