% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function conf_loss_struct = PoseSoftMaxLossForward(conf_pred, conf_gt)
    conf_loss_struct.pred_angle = SoftMax(double(conf_pred(1:8, :)));
    conf_loss_struct.pred_vec1 = SoftMax(double(conf_pred(9:16, :)));
    conf_loss_struct.pred_vec2 = SoftMax(double(conf_pred(17:24, :)));
    conf_loss_struct.pred_vec3 = SoftMax(double(conf_pred(25:32, :)));
    conf_loss_struct.label = conf_gt;

%     num_classes = size(conf_loss_struct.pred, 1);
%     loss = zeros(size(conf_loss_struct.pred, 2), 1);
%     for i_class = 1 : num_classes
%         loss(conf_gt == i_class - 1) = -log(max(conf_loss_struct.pred(i_class, ...
%                             conf_gt == i_class - 1), realmin));
%     end
    lossVec_angle = -log(conf_loss_struct.pred_angle(sub2ind(size(conf_loss_struct.pred_angle), ...
                                        conf_gt(1,:) + 1, (1 : length(conf_gt(1,:))))));
    lossVec_vec1 = -log(conf_loss_struct.pred_vec1(sub2ind(size(conf_loss_struct.pred_vec1), ...
                                        conf_gt(2,:) + 1, (1 : length(conf_gt(2,:))))));
    lossVec_vec2 = -log(conf_loss_struct.pred_vec2(sub2ind(size(conf_loss_struct.pred_vec2), ...
                                        conf_gt(3,:) + 1, (1 : length(conf_gt(3,:))))));
    lossVec_vec3 = -log(conf_loss_struct.pred_vec3(sub2ind(size(conf_loss_struct.pred_vec3), ...
                                        conf_gt(4,:) + 1, (1 : length(conf_gt(4,:))))));
    conf_loss_struct.loss = sum(lossVec_angle) + sum(lossVec_vec1) + sum(lossVec_vec2) + sum(lossVec_vec3);

end
