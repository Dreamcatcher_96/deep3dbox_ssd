% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期五 30/12/2016 10:22.
% Last Revision: 星期五 30/12/2016 10:22.
%qt

function ssd_net_opt = SSDNetOption()


    %% basic settings
    ssd_net_opt.batch_size = 1;
    ssd_net_opt.background_id = 0;
%     ssd_net_opt.mean_value = [104, 117, 123];
    ssd_net_opt.mean_value = [123, 117, 104];
    ssd_net_opt.data_size = 224;
    ssd_net_opt.max_iter = 60000;
    ssd_net_opt.step_size = 40000;
    ssd_net_opt.save_iter = 4000;
    ssd_net_opt.pos_overlap = 0.5;
    ssd_net_opt.neg_overlap = 0.5;
    ssd_net_opt.neg_pos_ratio = 3;
    ssd_net_opt.num_classes = 3;
    ssd_net_opt.base_lr = 0.0001;
    ssd_net_opt.lr_gamma = 0.1;
    ssd_net_opt.nms_overlap = 0.4;
    ssd_net_opt.min_score = 0.01;
    ssd_net_opt.test_keep = 400;
    ssd_net_opt.code_lambda_1 = 0.1;
    ssd_net_opt.code_lambda_2 = 0.2;


    %% files and pathes
    ssd_net_opt.solver_file = '/home/hanx/work/sina_face/Ori_SSD/models/Context_Net/solver.prototxt';
    ssd_net_opt.solver_file = '/home/hanx/work/sina_face/Ori_SSD/models/Context_Net/train.prototxt';
    ssd_net_opt.deploy_file = '/home/hanx/work/sina_face/Ori_SSD/models/Context_Net/deploy.prototxt';
    ssd_net_opt.init_file = '/home/hanx/work/sina_face/Ori_SSD/models/Context_Net/VGG_ILSVRC_16_layers_fc_reduced.caffemodel';


    %% batch sampler config
    batch_sampler(1).max_trials = 1;
    batch_sampler(1).ignore_area = 10;

    batch_sampler(2).max_trials = 50;
    batch_sampler(2).min_scale = 0.3;
    batch_sampler(2).max_scale = 1.0;
    batch_sampler(2).min_aspect_ratio = 0.5;
    batch_sampler(2).max_aspect_ratio = 2.0;
    batch_sampler(2).sample_constraint.min_jaccard_overlap = 0.1;
    batch_sampler(2).ignore_area = 10;

    batch_sampler(3).max_trials = 50;
    batch_sampler(3).min_scale = 0.3;
    batch_sampler(3).max_scale = 1.0;
    batch_sampler(3).min_aspect_ratio = 0.5;
    batch_sampler(3).max_aspect_ratio = 2.0;
    batch_sampler(3).sample_constraint.min_jaccard_overlap  = 0.3;
    batch_sampler(3).ignore_area = 10;

    batch_sampler(4).max_trials = 50;
    batch_sampler(4).min_scale = 0.3;
    batch_sampler(4).max_scale = 1.0;
    batch_sampler(4).min_aspect_ratio = 0.5;
    batch_sampler(4).max_aspect_ratio = 2.0;
    batch_sampler(4).sample_constraint.min_jaccard_overlap  = 0.5;
    batch_sampler(4).ignore_area = 10;

    batch_sampler(5).max_trials = 50;
    batch_sampler(5).min_scale = 0.3;
    batch_sampler(5).max_scale = 1.0;
    batch_sampler(5).min_aspect_ratio = 0.5;
    batch_sampler(5).max_aspect_ratio = 2.0;
    batch_sampler(5).sample_constraint.min_jaccard_overlap = 0.7;
    batch_sampler(5).ignore_area = 10;

    batch_sampler(6).max_trials = 50;
    batch_sampler(6).min_scale = 0.3;
    batch_sampler(6).max_scale = 1.0;
    batch_sampler(6).min_aspect_ratio = 0.5;
    batch_sampler(6).max_aspect_ratio = 2.0;
    batch_sampler(6).sample_constraint.min_jaccard_overlap = 0.9;
    batch_sampler(6).ignore_area = 10;

    batch_sampler(7).max_trials = 50;
    batch_sampler(7).min_scale = 0.3;
    batch_sampler(7).max_scale = 1.0;
    batch_sampler(7).min_aspect_ratio = 0.5;
    batch_sampler(7).max_aspect_ratio = 2.0;
    batch_sampler(7).sample_constraint.max_jaccard_overlap  = 1.0;
    batch_sampler(7).ignore_area = 10;

    ssd_net_opt.batch_sampler = batch_sampler;


    %% the default boxes
    % to aviod redundant computation we save the
    % coordinates of all the prior-boxes
    prior = load('./priorbox_3scale.mat');
    dbox = zeros(8, size(prior.priorbox, 1));
    dbox(1 : 4, :) = prior.priorbox.';
    dbox(5, :) = dbox(3, :) - dbox(1, :); % width
    dbox(6, :) = dbox(4, :) - dbox(2, :); % height
    dbox(7, :) = (dbox(1, :) + dbox(3, :)) ./ 2; % center x
    dbox(8, :) = (dbox(2, :) + dbox(4, :)) ./ 2; % center y
    clear('prior');
    ssd_net_opt.dbox = dbox;

end
