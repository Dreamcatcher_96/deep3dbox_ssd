% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function conf_loss_struct = SoftMaxLossForward(conf_pred, conf_gt)

    conf_loss_struct.pred  = SoftMax(double(conf_pred));
    conf_loss_struct.label = conf_gt;

%     num_classes = size(conf_loss_struct.pred, 1);
%     loss = zeros(size(conf_loss_struct.pred, 2), 1);
%     for i_class = 1 : num_classes
%         loss(conf_gt == i_class - 1) = -log(max(conf_loss_struct.pred(i_class, ...
%                             conf_gt == i_class - 1), realmin));
%     end
    lossVec = -log(conf_loss_struct.pred(sub2ind(size(conf_loss_struct.pred), ...
                                        conf_gt + 1, (1 : length(conf_gt))')));
    conf_loss_struct.loss = sum(lossVec);

end
