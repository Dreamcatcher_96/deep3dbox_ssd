% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 01/10/2016 19:09.
% Last Revision: 星期四 30/03/2017 20:20.
%qt
function SSD_Net_Test_GPU1()

    clc;
    clear;
    close all;
    dbstop if error;
    RandStream.setGlobalStream(RandStream('mt19937ar', 'Seed', 'shuffle'));
    addpath('./nms/');
    addpath('./dataArgu/');

    %% Initialization
    % caffe reset
    gpu_id = 1;
    use_gpu = true;
    CaffeRestart(gpu_id, use_gpu);

    % options for the network and dataset
    job_name = 'ori_ssd';
    net_opt = SSDNetOption();
    net_opt.init_file = '/home/alan/deep3dbox_ssd/models/hanxi_ssd_goodAug80000/CaffeModel_GPU2_DatasetVOC0712_RandSeed1982_MaxIter80000_StepSiz60000_PosOverlap0.5_NegOverlap0.5_NegPosR3_7.369555e+05/final_net_iter_80000.caffemodel';
%     net_opt.init_file = './models/hanxi_ssd_imgaug/CaffeModel_GPU2_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_60000.caffemodel';
%     net_opt.init_file = './models/finetuned_ssd/CaffeModel_GPU1_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_30000.caffemodel';
%     net_opt.deploy_file = './models/SSD_300x300_Ori/train.prototxt';
%     net_opt.init_file = './models/hanxi_ssd_imgaug_finetune/CaffeModel_GPU1_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_60000.caffemodel';
    net_opt.deploy_file = '/home/alan/deep3dbox_ssd/models/Ori_VGG16/train-mini.prototxt';

    dataset_name = 'voc0712';
    data_opt = DatasetOption(dataset_name);

    % the caching and save path
    sub_folder_name = ['CaffeModel_GPU', ...
        num2str(gpu_id), '_Dataset', upper(dataset_name), '_TrnTstR', ...
            sprintf('%02d', round(100 * data_opt.R)), '_TrnValR', ...
                        sprintf('%02d', round(100 * data_opt.R_val))];
    save_path = ['./results/', job_name, '/', sub_folder_name];
    MakeDirIfMissing(save_path);
    system('tar -cvvf mfiles.tar *.m');
    system(['mv mfiles.tar ', save_path]);

    % whethe show the inter-media results of training
    show_pause = 0;

    % network initialization
    test_net = caffe.Net(net_opt.deploy_file, 'test');
    test_net.copy_from(net_opt.init_file);

    % obtain the trianing, validation and test sets.    
    dataset_path = '/media/alan/Data/HQ_DBOutside/';
    dataset_year = 'DBInOut';
    dataset_usage = 'train';
    dataset_trn = GetDatasetNew(dataset_path, dataset_year, dataset_usage);
    
    [trn_img, trn_obj_bbox, trn_obj_class, trn_obj_quat, trn_obj_difficulty] = ...
                                            SSDTestImage(dataset_trn);
    
    dataset_path = '/media/alan/Data/HQ_DBOutside/';
    dataset_year = 'DBInOut';
    dataset_usage = 'test';
    dataset = GetDatasetNew(dataset_path, dataset_year, dataset_usage);
    all_class_name = {'DBOutside', 'DBInside'};
    all_class_color = DivideRGBSpace(length(all_class_name));
    num_class = length(all_class_name);

    % display the net and dataset infomation
    disp('net config:');
    disp(net_opt);
    disp(data_opt);


    %% test
    num_tst = numel(dataset);
    n_batch = ceil(num_tst ./ net_opt.batch_size);
    u = n_batch * net_opt.batch_size - num_tst;
    batch_idx_all = reshape([1 : num_tst, randi(num_tst, 1, u)], ...
                                    net_opt.batch_size, []);
    bbox_pred = cell(num_class, num_tst);
    score_pred = cell(num_class, num_tst);
    pose_pred = cell(num_class, num_tst);
    aboxes = cell(num_class, num_tst);
    ground_truth = cell(num_class, num_tst);
    pose_gt = cell(num_class, num_tst);
    obj_difficulty = cell(num_class, num_tst);
    been_tested = false(1, num_tst);
    all_img = cell(1, 1);
    for i_tst = 1 : 10%size(batch_idx_all, 2)

        fprintf('Hanxi SSD test start, batch-%d, %d images, ', ...
                                    i_tst, size(batch_idx_all, 1));
        tst_start = tic;

        % get the input of the ssd
        batch_idx = batch_idx_all(:, i_tst);
        batch_dataset = dataset(batch_idx);
        [tst_img, tst_obj_bbox, tst_obj_class, tst_obj_quat, tst_obj_difficulty] = ...
                                            SSDTestImage(batch_dataset);
        all_img = [all_img, tst_img];
        net_input_data = GetCaffeInput(tst_img, ...
                    tst_obj_bbox, tst_obj_class, tst_obj_quat, tst_obj_quat, net_opt);

        % forward
        time = FlexibleForward(net_input_data, test_net);
        disp(time)
        % detection inference
        mbox_loc = test_net.blobs('mbox_loc').get_data();
        mbox_conf = test_net.blobs('mbox_conf').get_data();
        multibin_loc = test_net.blobs('mbox_pose_loc').get_data();
        multibin_conf = test_net.blobs('mbox_pose_conf').get_data();
        loc_pred = reshape(mbox_loc, 4, [], net_opt.batch_size);
        conf_pred = reshape(mbox_conf,net_opt.num_classes, ...
                                            [], net_opt.batch_size);
        pose_loc_pred = reshape(multibin_loc, net_opt.num_classes-1, 32, [], net_opt.batch_size);
        pose_conf_pred = reshape(multibin_conf, net_opt.num_classes-1, 32, [], net_opt.batch_size);
        
        % detection inference
        bbox_pred_batch = cell(num_class, length(tst_img));
        score_pred_batch = cell(num_class, length(tst_img));
        pose_pred_batch = cell(num_class, length(tst_img));
        for i_img = 1 : length(tst_img)
            loc_pred_now = loc_pred(:, :, i_img);
            conf_pred_now = conf_pred(:, :, i_img);
            pose_conf_pred_now = pose_conf_pred(:, :, :, i_img);
            pose_loc_pred_now = pose_loc_pred(:, :, :, i_img);
            [bbox_pred_batch(:, i_img), score_pred_batch(:, i_img), pose_pred_batch(:, i_img)] = ...
                DetectionInference(tst_img{i_img}, net_opt.dbox, loc_pred_now, ...
                                                            conf_pred_now, pose_conf_pred_now, pose_loc_pred_now, net_opt);
        end

        fprintf(' ... done, took %2.3f sec.\n', toc(tst_start));
        
%         pose_all = zeros(16, 4);
%         for i = 1:16
%             score = score_pred_batch{i};
%             pose = pose_pred_batch{i};
%             [~, idx] = max(score);
%             pose_all(i, :) = pose(idx, :);
%         end
%         
%         pose_all__ = quat2rotm(pose_all);
%         diff = zeros(length(tst_img), length(tst_img));
%         max_idx = 0;
%         for i = 1:length(tst_img)
%             for j = 1:length(tst_img)
%                 if i == j
%                     diff(i, j) = inf;
%                 else
%                     diff(i, j) = RotDiff(pose_all__(:, :, i), pose_all__(:, :, j));
%                 end
%                 %                 if RotDiff(pose_all(:, :, i), pose_all(:, :, j)) < max_
% % %                     max_ = RotDiff(pose_all(:, :, i), pose_all(:, :, j));
% % %                     max_idx = j;
% %                 end
%             end
% %             temp = pose_all(:, :, i+1);
% %             pose_all(:, :, i+1) = pose_all(:, :, max_idx);
% %             pose_all(:, :, max_idx) = temp;
%         end
%         [~, idx] = sort(diff, 2);
        % save detection results
        bbox_pred(:, batch_idx) = bbox_pred_batch;
        score_pred(:, batch_idx) = score_pred_batch;
        pose_pred(:, batch_idx) = pose_pred_batch;
        for i_class = 1 : num_class
            aboxes(i_class, batch_idx) = cellfun(@(x,y,z) [x', y', z], ...
                bbox_pred_batch(i_class, :), score_pred_batch(i_class, :), pose_pred_batch(i_class, :), 'UniformOutput', false);
            this_class = cellfun(@(x) x == i_class, ...
                        tst_obj_class, 'UniformOutput', false);
            ground_truth(i_class, batch_idx) = ...
                cellfun(@(x,y) x(:, y)', tst_obj_bbox, this_class, 'UniformOutput', false);            
            pose_gt(i_class, batch_idx) = ...
                cellfun(@(x,y) x(:, y)', tst_obj_quat, this_class, 'UniformOutput', false);
            obj_difficulty(i_class, batch_idx) = cellfun(@(x,y) x(y), ...
                        tst_obj_difficulty, this_class, 'UniformOutput', false);
        end
        been_tested(batch_idx) = true;

        % show detection results
        if show_pause <= 0, continue; end

        hf = figure(1);
        for i_img = 1 : length(tst_img)
            bbox_pred_now = bbox_pred_batch(:, i_img);
            score_pred_now = score_pred_batch(:, i_img);
            subplot(1, 2, 1);
            ShowDetectionResults(hf, tst_img{i_img}, bbox_pred_now, ...
                score_pred_now, tst_obj_bbox{i_img}, tst_obj_class{i_img}, ...
                                        all_class_name, all_class_color);
            bbox_pred_now = bbox_pred_batch(:, idx(i_img, 1));
            score_pred_now = score_pred_batch(:, idx(i_img, 1));
            subplot(1, 2, 2);
            
            ShowDetectionResults(hf, tst_img{idx(i_img, 1)}, bbox_pred_now, ...
                score_pred_now, tst_obj_bbox{idx(i_img, 1)}, tst_obj_class{idx(i_img, 1)}, ...
                                        all_class_name(i_class), all_class_color);
                                    
            title(sprintf('RotDiff = %f', RotDiff(pose_all(:, :, i_img), pose_all(:, :, idx(i_img, 1)))));
            pause(show_pause);clf;
        end

    end
    aboxes(:, ~been_tested) = [];
    ground_truth(:, ~been_tested) = [];
    obj_difficulty(:, ~been_tested) = [];
    save([job_name, '_result.mat'], 'aboxes', ...
                        'ground_truth', 'obj_difficulty', '-v7.3');
%     load([job_name, '_result.mat']);
    diff = cell(1, 2);
    idx = cell(1, 2);
    pose_all_ = cell(1, 2);
    pose_gt_ = cell(1, 2);
    for i_class = 1:num_class
        pose_all_{i_class} = cellfun(@(x) x(x(:, 5) == max(x(:, 5)), 6:end), aboxes(i_class, :), 'UniformOutput', false);
        pose_all_{i_class} = pose_all_{i_class}(~cellfun(@isempty, pose_all_{i_class}));
        pose_all_{i_class} = cell2mat(pose_all_{i_class}');
        pose_all_{i_class} = quat2rotm(pose_all_{i_class});
        pose_gt_{i_class} = cell2mat(trn_obj_quat(cellfun(@(x) x == i_class, trn_obj_class)));
        pose_gt_{i_class} = quat2rotm(pose_gt_{i_class}');
        diff{i_class} = zeros(size(pose_all_{i_class}, 3), size(pose_gt_{i_class}, 3));
        for i = 1:size(pose_all_{i_class}, 3)
            for j = 1:size(pose_gt_{i_class}, 3)
                diff{i_class}(i, j) = RotDiff(pose_all_{i_class}(:, :, i), pose_gt_{i_class}(:, :, j));
            end
            fprintf('%d/%d\n', i, j);
        end
        [~, idx{i_class}] = sort(diff{i_class}, 2);
    end
    all_img = all_img(2:end);
    hf = figure(1);
    for i_img = 1 : length(dataset)
        bbox_pred_now = cellfun(@(x) x(:, 1:4)', aboxes(:, i_img), 'UniformOutput', false);
%         bbox_pred_now = mat2cell(bbox_pred_now, 4);
        score_pred_now = cellfun(@(x) x(:, 5)', aboxes(:, i_img), 'UniformOutput', false);
%         score_pred_now = mat2cell(score_pred_now, 1);
        ShowDetectionResults(hf, all_img{i_img}, bbox_pred_now, ...
            score_pred_now, ...
                                    all_class_name, all_class_color);
        for i_class = 1:num_class
            select_idx = score_pred_now{i_class} > 0.5;
            if numel(find(select_idx)) == 0
                continue
            end
            fi = figure(i_class+1);
            bbox_pred_gt = trn_obj_bbox(cellfun(@(x) x == i_class, trn_obj_class));
            bbox_pred_gt = bbox_pred_gt(idx{i_class}(i_img, 1));
            score_pred_gt = trn_obj_class(cellfun(@(x) x == i_class, trn_obj_class));
            score_pred_gt = score_pred_gt(idx{i_class}(i_img, 1));
            current_img = trn_img(cellfun(@(x) x == i_class, trn_obj_class));
            current_img = current_img{idx{i_class}(i_img, 1)};
            ShowDetectionResults(fi, current_img, bbox_pred_gt, ...
                score_pred_gt, ...
                                        all_class_name(i_class), all_class_color);

            title(sprintf('RotDiff = %f', RotDiff(pose_all_{i_class}(:, :, i_img), pose_gt_{i_class}(:, :, idx{i_class}(i_img, 1)))));
        end
        pause(1);clf;
    end
    [ap_all, pose_all] = DetectionResEval(aboxes, ground_truth, pose_gt, ...
            obj_difficulty, all_class_name, all_class_name, net_opt.nms_overlap);
    disp(mean(ap_all));
    disp(mean(pose_all));

end
