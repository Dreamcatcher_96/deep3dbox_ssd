function delta = RotDiff(R1,R2)
    delta = norm(logm(R1'*R2),'fro')/sqrt(2) * 180 / pi;
end