% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function conf_loss_struct = SoftMaxLossBackward(conf_loss_struct)

    conf_loss_struct.pred_diff = conf_loss_struct.pred;

    num_class = size(conf_loss_struct.pred, 1);
    for i_class = 1 : num_class
        cur_class_idx = conf_loss_struct.label == i_class - 1;
        conf_loss_struct.pred_diff(i_class, cur_class_idx) = ...
                conf_loss_struct.pred_diff(i_class, cur_class_idx) - 1;
    end

end
