% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function [ loc_loss_struct ] = SmoothL1Backward( loc_loss_struct )
    abs_diff_ge_1_idx = find(abs(loc_loss_struct.diff) >= 1);
    loc_loss_struct.loc_pred_diff = loc_loss_struct.diff;
    loc_loss_struct.loc_pred_diff(abs_diff_ge_1_idx) = ...
                        sign(loc_loss_struct.diff(abs_diff_ge_1_idx));
end
