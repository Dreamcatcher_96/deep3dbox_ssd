% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期三 11/01/2017 13:47.
% Last Revision: 星期三 11/01/2017 13:47.
%qt

function bbox = MapBackBBox(bbox, im_siz)

    bbox([1, 3], :) = bbox([1, 3], :) .* im_siz(2);
    bbox([2, 4], :) = bbox([2, 4], :) .* im_siz(1);

end



