% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function [ loc_loss_struct ] = SmoothL1Forward( loc_pred_data, loc_gt_data )

    loc_loss_struct.diff    = zeros(size(loc_pred_data));
    loc_loss_struct.errors  = zeros(size(loc_pred_data));

    loc_loss_struct.diff    = loc_pred_data - loc_gt_data;

    abs_diff                = abs( loc_loss_struct.diff );

    abs_val_lt_1            = find( abs_diff < 1 );
    abs_val_ge_1            = find( abs_diff >= 1);

    loc_loss_struct.errors(abs_val_lt_1) = power( abs_diff(abs_val_lt_1), 2 ) * 0.5;
    loc_loss_struct.errors(abs_val_ge_1) = abs_diff(abs_val_ge_1) - 0.5;

    loc_loss_struct.loss    = sum( abs(loc_loss_struct.errors(:)) );
end
