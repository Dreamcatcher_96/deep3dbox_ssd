% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期四 29/12/2016 10:46.
% Last Revision: 星期四 29/12/2016 10:46.
%qt

function CaffeRestart(gpu_id, use_gpu)

    global process_start;

    if process_start
        caffe.reset_all();
    else
        clear is_valid_handle; % to clear init_key
%         gpuDevice([]);
        gpuDevice(gpu_id);
        caffe.set_device(gpu_id-1);
        caffe.reset_all();
        if use_gpu
            caffe.set_mode_gpu();
        else
            caffe.set_mode_cpu();
        end
        process_start = 1;
%         fid = fopen('./flag_caffe_initialized', 'w');
%         fprintf(fid, 'caffe has been initialized!\n');
%         fclose(fid);
    end

end



