% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 01/10/2016 19:09.
% Last Revision: 星期四 12/01/2017 14:00.
%qt

function max_scores = SSDMaxScore(conf_data)

%     max_val = max(conf_data, [], 1);
%     max_val_pos = max(conf_data(2 : end, :, :), [], 1);
%     sum_val = sum(exp(conf_data - repmat(max_val, size(conf_data, 1), 1)), 1);
%     max_scores = squeeze(exp(max_val_pos - max_val) ./ sum_val)';
    
    [~, num_dbox, siz_batch] = size(conf_data);
    max_scores = zeros(siz_batch, num_dbox);
    for i_batch = 1 : siz_batch
        conf_data_now = SoftMax(conf_data(:, :, i_batch)); % 计算分类结果的softmax
        max_scores(i_batch, :) = max(conf_data_now(2 : end, :), [], 1); % 找到每个bbox中除背景类以外最大的loss
    end

end
