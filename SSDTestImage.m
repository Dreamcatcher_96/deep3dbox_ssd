% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期五 17/02/2017 15:02.
% Last Revision: 星期五 17/02/2017 15:02.

function [tst_img, tst_obj_bbox, tst_obj_class, tst_obj_quat, tst_obj_difficulty] = ...
                                                SSDTestImage(dataset)

    img_num = numel(dataset);
    tst_img = cell(1, img_num);
    tst_obj_bbox = cell(1, img_num);
    tst_obj_class = cell(1, img_num);
    tst_obj_difficulty = cell(1, img_num);
    tst_obj_quat = cell(1, img_num);
    for i_im = 1 : img_num

%         im_name = [data_opt.image_root, '/', dataset{i_im}.filename];
        im_name = dataset{i_im}.filename;
        im = imread(im_name);
        obj = dataset{i_im}.object;

        num_obj = numel(obj);
        obj_bbox = zeros(4, num_obj);
        obj_class = zeros(1, num_obj);
        obj_difficulty = zeros(1, num_obj);
        obj_quat= zeros(4, num_obj);
        for i_bbox = 1 : num_obj
            bbox_now = [min(obj(i_bbox).bndbox(:,1)); min(obj(i_bbox).bndbox(:,2)); max(obj(i_bbox).bndbox(:,1)); max(obj(i_bbox).bndbox(:,2))];
            obj_bbox(:, i_bbox) = bbox_now';
            obj_class(i_bbox) = obj(i_bbox).class_id;
            obj_difficulty(i_bbox) = obj(i_bbox).difficult;
            R = obj(i_bbox).RT(1:3, 1:3);
            obj_quat(:, i_bbox) = rotm2quat(R);
        end

        tst_img{i_im} = im;
        tst_obj_bbox{i_im} = obj_bbox;
        tst_obj_class{i_im} = obj_class;
        tst_obj_difficulty{i_im} = obj_difficulty;
        tst_obj_quat{i_im} = obj_quat;
    end

end




