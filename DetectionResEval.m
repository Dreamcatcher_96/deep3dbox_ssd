function [class_ap, class_pose_acc, class_prec, class_rec] = DetectionResEval(aboxes, ...
            ground_truth, pose_gt, obj_difficulty, classes, class_pick, nms_thres)

    thresh_ap = 0.01;
    minoverlap = 0.5;
    pose_thresh = 15;
    A = cell2mat(cellfun(@(x) strcmpi(x, classes(:)'), ...
                        class_pick(:), 'UniformOutput', 0));
    class_idx = find(any(A, 1));
    num_img = size(ground_truth, 2);

    class_rec = cell(1, length(classes));
    class_prec = cell(1, length(classes));
    class_ap =  zeros(1, length(classes));
    class_pose_acc =  zeros(1, length(classes));
    
    for i_class = class_idx
        predict = [];

        gt_now = ground_truth(i_class, :);
        gt_detected = cell(1, num_img);
        npos = 0;
        for i_img = 1 : num_img
            gt_detected{i_img} = false(1, size(gt_now{i_img}, 1));
            npos = npos + sum(~obj_difficulty{i_class, i_img}); % size(gt_now{i_img}, 1);
        end
        aboxes_now = aboxes(i_class, :);

        is_empty = cellfun(@isempty, aboxes_now);
        aboxes_now(is_empty) = [];

        img_idx = num2cell(find(~is_empty));
        ids = cellfun(@(x,y) y * ones(size(x, 1), 1), ...
                        aboxes_now, img_idx, 'UniformOutput', false);
        ids = cell2mat(ids(:));

        aboxes_now = cell2mat(aboxes_now(:));
        if isempty(aboxes_now)
            if all(cellfun(@isempty, gt_now))
                fprintf('\nFor %s with overlap %2.2f, AP = 1.\n', ...
                                    classes{i_class}, nms_thres);
            else
                fprintf('\nFor %s with overlap %2.2f, AP = 0.\n', ...
                                    classes{i_class}, nms_thres);
            end
            continue;
        end

        small_idx = aboxes_now(:, 5) < thresh_ap;
        aboxes_now(small_idx, :) = [];
        ids(small_idx) = [];
        gtids = 1 : num_img;

        confidence = aboxes_now(:, 5);
        BB = aboxes_now(:, 1 : 4)';
        pose = aboxes_now(:, 6:end)';

        % sort detections by decreasing confidence
        [~, si] = sort(-confidence);
        ids = ids(si);
        BB = BB(:, si);
        pose = pose(:, si);

        % assign detections to ground truth objects
        cls = classes{i_class};
        nd = length(confidence);
        tp = zeros(nd, 1);
        fp = zeros(nd,1);

        tic;
        for d = 1 : nd
            % display progress
            if toc>1
                fprintf('%s: pr: compute: %d/%d\n', cls, d, nd);
                drawnow;
                tic;
            end

            % find ground truth image
            i_img = find(gtids == ids(d));
            if isempty(i_img)
                error('unrecognized image "%s"',ids{d});
            elseif length(i_img)>1
                error('multiple image "%s"',ids{d});
            end

            % assign detection to ground truth object if any
            bb = BB(:,d);
            pose_now = pose(:, d);
            ovmax = -inf;
            for i_bbox = 1 : size(gt_now{i_img}, 1)
                bbgt = gt_now{i_img}(i_bbox, :)';
                bi = [max(bb(1), bbgt(1)); max(bb(2), bbgt(2)); ...
                                min(bb(3), bbgt(3)); min(bb(4), bbgt(4))];
                iw = bi(3) - bi(1) + 1;
                ih = bi(4) - bi(2) + 1;
                if iw > 0 && ih > 0
                    % compute overlap as area of intersection / area of union
                    ua=(bb(3)-bb(1)+1)*(bb(4)-bb(2)+1)+...
                       (bbgt(3)-bbgt(1)+1)*(bbgt(4)-bbgt(2)+1)-...
                       iw*ih;
                    ov=iw*ih/ua;
                    if ov>ovmax
                        ovmax=ov;
                        jmax=i_bbox;
                    end
                end
            end
            % assign detection as true positive/don't care/false positive
            if ovmax >= minoverlap
                if ~obj_difficulty{i_class, i_img}(jmax)
                    if ~gt_detected{i_img}(jmax)
                        pose_gt_now = pose_gt{i_class, i_img}(jmax, :);
                        Rotation_pred = quat2rotm(pose_now');
                        Rotation_gt = quat2rotm(pose_gt_now);

                        delta = RotDiff(Rotation_pred, Rotation_gt);
                        if delta < pose_thresh                            
                            predict(end+1) = 1;            % true positive      
                        else                            
                            predict(end+1) = 0;
                        end
                        tp(d)=1;
                        gt_detected{i_img}(jmax)=true;
                    else
                        fp(d)=1;            % false positive (multiple detection)
                    end
                end
            else
                fp(d)=1;                    % false positive
            end
        end

        % compute precision/recall
        fp=cumsum(fp);
        tp=cumsum(tp);
        rec=tp/npos;
        prec=tp./(fp+tp);

        % compute average precision
        ap = 0;
        for t = 0 : 0.1 : 1
            p=max(prec(rec>=t));
            if isempty(p), p=0; end
            ap=ap+p/11;
        end
        pose_acc = sum(predict(:)) / numel(predict);
        if isnan(pose_acc)
            pose_acc = 0;
        end
        % plot precision/recall
%         plot(rec,prec,'-'); grid;
%         xlabel 'recall'; ylabel 'precision';
%         title(sprintf('class: %s, AP = %.3f', cls, ap));
%         img_path = [res_folder, db_name, '_DetectRes_', 'ImgNum', num2str(num_img), ...
%             '_Thresh', num2str(round(thresh * 100)), '_SameClassOverlap', ...
%                 num2str(round(sameclass_nums_thres * 10)), '_Obj', classes{i_class}, '_AP.png'];
%         saveas(gcf, img_path);pause(0.2);clf;
        class_pose_acc(i_class) = pose_acc;
        class_rec{i_class} = rec;
        class_prec{i_class} = prec;
        class_ap(i_class) = ap;

%         fprintf('\nFor %s with overlap %2.2f, AP = %2.3f.\n', ...
%                             classes{i_class}, nms_thres, ap);

    end

%     ap_path = [res_folder, db_name, '_DetectRes_', 'ImgNum', num2str(num_img), ...
%                     '_Thresh', num2str(round(thresh * 100)), '_AP.mat'];
%     save(ap_path, 'class_ap', 'class_prec', 'class_rec', 'classes', '-v7.3');

end