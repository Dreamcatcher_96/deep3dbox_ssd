% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 18/02/2017 21:12.
% Last Revision: 星期六 18/02/2017 21:12.

function forward_time = FlexibleForward(net_input_data, cnn_net)

    input_shape = size(net_input_data);
    if length(input_shape) == 3
        input_shape = [input_shape, 1];
    end
    cnn_net.blobs('data').reshape(input_shape);
    forward_time = tic;
    cnn_net.forward({net_input_data});
    forward_time = toc(forward_time);
end



