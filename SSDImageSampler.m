% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 08/01/2017 13:56.
% Last Revision: 星期日 08/01/2017 13:56.
%qt

function [sub_img, subimg_obj_bbox, subimg_obj_class, subimg_obj_conf, subimg_obj_loc, subimg_obj_difficulty, subimg_obj_quat] = ...
                SSDImageSampler(dataset, batch_sampler, data_opt)

    img_num = numel(dataset);
    num_sample = numel(batch_sampler);
    sub_img = cell(1, img_num);
    subimg_obj_bbox = cell(1, img_num);
    subimg_obj_class = cell(1, img_num);
    subimg_obj_conf = cell(1, img_num);
    subimg_obj_loc = cell(1, img_num);
    subimg_obj_difficulty = cell(1, img_num);
    subimg_obj_quat = cell(1, img_num);
    for i_im = 1 : img_num

        % read the image and the bounding-boxes from dataset
        [im, obj_bbox, obj_class, obj_conf, obj_loc, obj_difficulty, obj_quat] = ReadDetDataset(dataset{i_im}); 

        % image random augmentation
        [im, obj_bbox] = ImageAugmentation(im, obj_bbox, data_opt);
        im_siz = size(im);

        % normalize the bbox
        mapped_obj_bbox = bsxfun(@rdivide, obj_bbox, ...
                    [im_siz(2); im_siz(1); im_siz(2); im_siz(1)]);

        % randomly sample the image
        rand_sampler_idx = randperm(num_sample);
        for i_s = 1 : num_sample

            sampler = batch_sampler(rand_sampler_idx(i_s));

            % sample some sub-images
            if isempty(sampler.min_scale)
                mapped_subimg_bbox = [0; 0; 1; 1];
            else
                mapped_subimg_bbox = RandomSampleImage(sampler, mapped_obj_bbox);
            end

            if isempty(mapped_subimg_bbox), continue; end

            % map the bbox to the real coordinate
            subimg_bbox = MapBackBBox(mapped_subimg_bbox, im_siz);
            % regularize the sub-image bbox
            subimg_bbox = RegularizeBBox(round(subimg_bbox), im_siz);
            % crop according to the bbox
            sub_img{i_im} = im(subimg_bbox(2) : subimg_bbox(4), ...
                                    subimg_bbox(1) : subimg_bbox(3), :);

            % the object bbox in the sub-image
            [subimg_obj_bbox{i_im}, valid_bbox_idx] = ...
                    SubImageBBox(obj_bbox, subimg_bbox, sampler.ignore_area);
            subimg_obj_bbox{i_im} = subimg_obj_bbox{i_im}(:, valid_bbox_idx);
            subimg_obj_class{i_im} = obj_class(valid_bbox_idx);
            subimg_obj_conf{i_im} = obj_conf(:, valid_bbox_idx);
            subimg_obj_loc{i_im} = obj_loc(:, valid_bbox_idx);
            subimg_obj_difficulty{i_im} = obj_difficulty(:, valid_bbox_idx);
            subimg_obj_quat{i_im} = obj_quat(:, valid_bbox_idx);

            if ~isempty(subimg_obj_class{i_im}), break; end

        end

    end

%     is_empty = cellfun(@isempty, subimg_obj_class);
%     sub_img(is_empty) = [];
%     subimg_obj_class(is_empty) = [];
%     subimg_obj_conf(is_empty) = [];
%     subimg_obj_loc(is_empty) = [];
%     subimg_obj_bbox(is_empty) = [];
%     subimg_obj_difficulty(is_empty) = [];
%     subimg_obj_quat(is_empty) = [];

end

%         im_name = [data_opt.image_root, '/', dataset{i_im}.filename];
%         im = imread(im_name);
%         obj = dataset{i_im}.object;
%         im_siz = size(im);

%         num_obj = numel(obj);
%         obj_bbox = zeros(4, num_obj);
%         obj_class = zeros(1, num_obj);
%         for i_bbox = 1 : num_obj
%             bbox_now = obj(i_bbox).bndbox;
%             obj_bbox(:, i_bbox) = [bbox_now.xmin; ...
%                             bbox_now.ymin; bbox_now.xmax; bbox_now.ymax];
%             obj_class(i_bbox) = obj(i_bbox).class_id;
%         end

%         datum           = dataset{i_im};
%         height          = datum.sampled_siz(1);
%         width           = datum.sampled_siz(2);

%         ratio_h         = height / target_siz;
%         ratio_w         = width  / target_siz;

%         I               = imresize(datum.sampled_datum, [target_siz, target_siz]);
%         I_cpy           = I;
%         I(:, :, 1)      = I(:, :, 1) - mean_vec(1);
%         I(:, :, 2)      = I(:, :, 2) - mean_vec(2);
%         I(:, :, 3)      = I(:, :, 3) - mean_vec(3);

%         data{i_im}        = I(:, :, [3, 2, 1]);

%         roi                 = get_ground_truth(datum.sampled_roi);
%         roi(:, 1:2:end-1)   = roi(:, 1:2:end-1) ./ ratio_w ./ target_siz;
%         roi(:, 2:2:end-1)   = roi(:, 2:2:end-1) ./ ratio_h ./ target_siz;

%         label{i_im}           = roi;

