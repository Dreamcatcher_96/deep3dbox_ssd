% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 12/03/2017 18:38.
% Last Revision: 星期日 12/03/2017 18:38.

function bbox_ltrb = LTWH2LTRB(bbox_ltwh)

    bbox_ltrb = bbox_ltwh;
    bbox_ltrb(3, :) = bbox_ltwh(3, :) + bbox_ltrb(1, :);
    bbox_ltrb(4, :) = bbox_ltwh(4, :) + bbox_ltrb(2, :);

end



