function MergeDataset()
    data1 = load('/media/alan/Data/DBOutside_DATA/DBOutside_train.mat');
    data2 = load('/media/alan/Data/DBInside_DATA/DBInside_train.mat');
%     data3 = load('/media/alan/Data/Data/VOCDataset/voc0712_trainval_transferred.mat');
%     for i = 1:numel(data3.dataset)
%        data3.dataset{i}.object = []; 
%     end
    dataset = [data1.dataset, data2.dataset];%, data3.dataset];
    dataset = dataset(randperm(numel(dataset)));
    save('DBInOut_train.mat', 'dataset');
    data1 = load('/media/alan/Data/DBOutside_DATA/DBOutside_test.mat');
    data2 = load('/media/alan/Data/DBInside_DATA/DBInside_test.mat');
%     data3 = load('/media/alan/Data/Data/VOCDataset/VOCdevkit2007/voc2007_test_transferred.mat');
%     for i = 1:numel(data3.dataset)
%        data3.dataset{i}.object = []; 
%     end
    dataset = [data1.dataset, data2.dataset];%, data3.dataset];
    dataset = dataset(randperm(numel(dataset)));
    save('DBInOut_test.mat', 'dataset');
end