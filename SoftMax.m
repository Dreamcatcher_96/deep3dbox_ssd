% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期三 15/03/2017 15:26.
% Last Revision: 星期三 15/03/2017 15:26.

function conf_pred = SoftMax(conf_pred)

    %  softmax
    num_classes = size(conf_pred, 1);
    max_val = max(conf_pred, [], 1); % 防止边界误差
    conf_pred = conf_pred - repmat(max_val, num_classes, 1);

    exp_conf_pred = exp(conf_pred);
    sum_exp_conf_pred = sum(exp_conf_pred, 1);
    conf_pred = exp_conf_pred ./ repmat(sum_exp_conf_pred, num_classes, 1);

end


