% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期四 30/03/2017 14:58.
% Last Revision: 星期四 30/03/2017 14:58.

function TransferVOCDataset()

    clear;
    clc;
    close all;

    dataset_path = '/media/alan/Data/Data/VOCDataset/VOCdevkit2007/';
    dataset_year = 'voc2007';
    dataset_usage = 'test';
%     dataset_path = '/media/alan/Data/Data/VOCDataset/';
%     dataset_year = 'voc0712';
%     dataset_usage = 'trainval';
    dataset = [];
    load([dataset_path, dataset_year, '_', dataset_usage, '.mat']);

    switch dataset_usage
    case 'test'
        roidb = dataset.roidb_test;
        imdb = dataset.imdb_test;
    case {'train', 'trainval'}
        roidb = dataset.roidb_train;
        imdb = dataset.imdb_train;
    otherwise
    end
    clear('dataset');

    if ~iscell(roidb)
        roidb = {roidb};
    end
    if ~iscell(imdb)
        imdb = {imdb};
    end
    
    offset = 0;
    count = 0;
    for i_db = 1 : length(imdb)
        num_img = length(imdb{i_db}.image_ids);
        if i_db ~= 1
            offset = size(dataset, 2);
            dataset = cat(2, dataset, cell(1, num_img));
        else
            dataset = cell(1, num_img);
        end
        for i_im = 1 : num_img
            num_obj = length(roidb{i_db}.rois(i_im).gt);
            flag = 0;
            for i_obj = 1 : num_obj
                if roidb{i_db}.rois(i_im).class(i_obj) == 5
                    flag = 1;
                    break;
                end
            end
            if flag == 1
                count = count + 1;
                continue;
            end
            dataset{i_im + offset}.filename = [imdb{i_db}.image_dir, ...
                                '/', imdb{i_db}.image_ids{i_im}, '.jpg'];
            dataset{i_im + offset}.object = struct('bndbox', cell(1, num_obj), ...
                'class_id', cell(1, num_obj), 'difficult', cell(1, num_obj));
            for i_obj = 1 : num_obj
                dataset{i_im + offset}.object(i_obj).bndbox = ...
                                roidb{i_db}.rois(i_im).boxes(i_obj, :)';
                dataset{i_im + offset}.object(i_obj).class_id = ...
                                roidb{i_db}.rois(i_im).class(i_obj);
                dataset{i_im + offset}.object(i_obj).difficult = ...
                                roidb{i_db}.rois(i_im).difficult(i_obj);
            end
        end
    end
    dataset(cellfun(@isempty,dataset)) = [];
    save([dataset_path, dataset_year, '_', dataset_usage, '_transferred.mat'], 'dataset');

end



