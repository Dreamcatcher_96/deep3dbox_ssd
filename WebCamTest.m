function WebCamTest()
    clc;
    clear;
    close all;
    dbstop if error;
    RandStream.setGlobalStream(RandStream('mt19937ar', 'Seed', 'shuffle'));
    addpath('./nms/');
    addpath('./dataArgu/');
    
    all_class_name = {'In', 'Out'};
    all_class_color = DivideRGBSpace(length(all_class_name));
    
    gpu_id = 2;
    use_gpu = true;
    CaffeRestart(gpu_id, use_gpu);
        
    net_opt = SSDNetOption();
    net_opt.init_file = '/home/alan/deep3dbox_ssd/models/hanxi_ssd_goodAug80000/CaffeModel_GPU2_DatasetVOC0712_RandSeed1982_MaxIter80000_StepSiz60000_PosOverlap0.5_NegOverlap0.5_NegPosR3_7.369376e+05/final_net_iter_80000.caffemodel';
    net_opt.deploy_file = '/home/alan/deep3dbox_ssd/models/hanxi_ssd_goodAug80000/CaffeModel_GPU2_DatasetVOC0712_RandSeed1982_MaxIter80000_StepSiz60000_PosOverlap0.5_NegOverlap0.5_NegPosR3_7.369376e+05/train-mini.prototxt';
    
    test_net = caffe.Net(net_opt.deploy_file, 'test');
    test_net.copy_from(net_opt.init_file);
    
    img_path = '/media/alan/Data/DBInside_DATA/DBInside_imgs/DBInside_12/';
    imgs = dir(img_path);
    imgs = imgs(3:end);
    hf = figure(1);
    for i = 1:numel(imgs)
        img = imread([img_path, imgs(i).name]);
        img = imresize(img, 0.5);
        caffe_data = CaffeInputPreProcess({img}, ...
            net_opt.mean_value, 'VGG', net_opt.data_size);
        FlexibleForward(caffe_data, test_net);
        mbox_loc = test_net.blobs('mbox_loc').get_data();
        mbox_conf = test_net.blobs('mbox_conf').get_data();
        multibin_loc = test_net.blobs('mbox_pose_loc').get_data();
        multibin_conf = test_net.blobs('mbox_pose_conf').get_data();
        loc_pred = reshape(mbox_loc, 4, [], 1);
        conf_pred = reshape(mbox_conf, 3, ...
                                            [], 1);
        pose_loc_pred = reshape(multibin_loc, 32, [], 1);
        pose_conf_pred = reshape(multibin_conf, 32, [], 1);
        
        loc_pred_now = loc_pred(:, :, 1);
        conf_pred_now = conf_pred(:, :, 1);
        pose_conf_pred_now = pose_conf_pred(:, :, 1);
        pose_loc_pred_now = pose_loc_pred(:, :, 1);
        [bbox_pred_batch, score_pred_batch, ~] = ...
            DetectionInference(img, net_opt.dbox, loc_pred_now, ...
                                                        conf_pred_now, pose_conf_pred_now, pose_loc_pred_now, net_opt);
        ShowDetectionResults(hf, img, bbox_pred_batch, ...
                score_pred_batch, all_class_name, all_class_color);
        pause(0.00001);
    end
end