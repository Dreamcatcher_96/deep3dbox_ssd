% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期三 11/01/2017 14:25.
% Last Revision: 星期三 11/01/2017 14:25.
%qt

function [subimg_obj_bbox, valid_bbox_idx] = SubImageBBox(obj_bbox, subimg_bbox, ignore_area)

    offset = subimg_bbox([1, 2, 1, 2]);
    subimg_obj_bbox = bsxfun(@minus, obj_bbox, offset(:));

    subimg_siz = [subimg_bbox(4) - subimg_bbox(2) + 1, ...
                  subimg_bbox(3) - subimg_bbox(1) + 1];

    subimg_obj_bbox = RegularizeBBox(round(subimg_obj_bbox), subimg_siz);

    bbox_w = (subimg_obj_bbox(3, :) - subimg_obj_bbox(1, :));
    bbox_h = (subimg_obj_bbox(4, :) - subimg_obj_bbox(2, :));
    bbox_area = bbox_w .* bbox_h;

    valid_bbox_idx = find(bbox_area > ignore_area & bbox_w > 0 & bbox_h > 0);

end



