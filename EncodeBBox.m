% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function encode_bbox = EncodeBBox(dbox, bbox, net_opt)

    % dbox  bbox  --> xmin; ymin; xmax; ymax;
%     dbox(5, :) = dbox(3, :) - dbox(1, :); % width
%     dbox(6, :) = dbox(4, :) - dbox(2, :); % height
%     dbox(7, :) = (dbox(1, :) + dbox(3, :)) ./ 2; % center x
%     dbox(8, :) = (dbox(2, :) + dbox(4, :)) ./ 2; % center y

    bbox_width = bbox(3, :) - bbox(1, :);
    bbox_height = bbox(4, :) - bbox(2, :);
    bbox_center_x = (bbox(1, :) + bbox(3, :)) / 2;
    bbox_center_y = (bbox(2, :) + bbox(4, :)) / 2;

    encode_bbox(1, :) = (bbox_center_x - dbox(7, :)) ./ dbox(5, :) ./ net_opt.code_lambda_1;
    encode_bbox(2, :) = (bbox_center_y - dbox(8, :)) ./ dbox(6, :) ./ net_opt.code_lambda_1;
    encode_bbox(3, :) = log(bbox_width ./ dbox(5, :)) ./ net_opt.code_lambda_2;
    encode_bbox(4, :) = log(bbox_height ./ dbox(6, :)) ./ net_opt.code_lambda_2;

end

%     prior_width              = dbox(3, :) - dbox(1, :);
%     prior_height             = dbox(4, :) - dbox(2, :);
%     prior_center_x           = (dbox(1, :) + dbox(3, :)) / 2;
%     prior_center_y           = (dbox(2, :) + dbox(4, :)) / 2;





