% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      30/12/2016 10:22.
% Last Revision: 30/12/2016 10:22.
%qt

function dataset = GetDatasetNew(dataset_path, dataset_year, dataset_usage)

    load([dataset_path, dataset_year, '_', dataset_usage, '.mat']);

end
