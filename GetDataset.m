% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      30/12/2016 10:22.
% Last Revision: 30/12/2016 10:22.
%qt

function dataset = GetDataset(data_opt)

    data_annotation = load(data_opt.annotation_file);

    if ~exist(data_opt.train_test_list_file, 'file')

        img_num = numel(data_annotation.data_list);
        rnd_idx = randperm(img_num);

        train_idx = rnd_idx(1 : ceil(img_num * data_opt.R));
        test_idx = rnd_idx(numel(train_idx) + 1 : end);

        train_num = numel(train_idx);
        val_num = ceil(train_num * data_opt.R_val);

        val_idx = train_idx(1 : val_num);
        train_idx(1 : val_num) = [];
        save(data_opt.train_test_list_file, ...
            'train_idx', 'test_idx', 'val_idx', '-v7.3');

    else

        load(data_opt.train_test_list_file);

    end

    dataset.data_train = data_annotation.data_list(train_idx);
    dataset.data_test = data_annotation.data_list(test_idx);
    dataset.data_val = data_annotation.data_list(val_idx);

end
