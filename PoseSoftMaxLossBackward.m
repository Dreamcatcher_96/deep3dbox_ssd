% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function conf_loss_struct = PoseSoftMaxLossBackward(conf_loss_struct)

    conf_loss_struct.pred_diff_angle = conf_loss_struct.pred_angle;
    conf_loss_struct.pred_diff_vec1 = conf_loss_struct.pred_vec1;
    conf_loss_struct.pred_diff_vec2 = conf_loss_struct.pred_vec2;
    conf_loss_struct.pred_diff_vec3 = conf_loss_struct.pred_vec3;

    num_class = size(conf_loss_struct.pred_angle, 1);
    for i_class = 1 : num_class
        cur_class_idx = conf_loss_struct.label(1, :) == i_class - 1;
        conf_loss_struct.pred_diff_angle(i_class, cur_class_idx) = ...
                conf_loss_struct.pred_diff_angle(i_class, cur_class_idx) - 1;
    end
    num_class = size(conf_loss_struct.pred_vec1, 1);
    for i_class = 1 : num_class
        cur_class_idx = conf_loss_struct.label(2, :) == i_class - 1;
        conf_loss_struct.pred_diff_vec1(i_class, cur_class_idx) = ...
                conf_loss_struct.pred_diff_vec1(i_class, cur_class_idx) - 1;
    end
    num_class = size(conf_loss_struct.pred_vec2, 1);
    for i_class = 1 : num_class
        cur_class_idx = conf_loss_struct.label(3, :) == i_class - 1;
        conf_loss_struct.pred_diff_vec2(i_class, cur_class_idx) = ...
                conf_loss_struct.pred_diff_vec2(i_class, cur_class_idx) - 1;
    end
    num_class = size(conf_loss_struct.pred_vec3, 1);
    for i_class = 1 : num_class
        cur_class_idx = conf_loss_struct.label(4, :) == i_class - 1;
        conf_loss_struct.pred_diff_vec3(i_class, cur_class_idx) = ...
                conf_loss_struct.pred_diff_vec3(i_class, cur_class_idx) - 1;
    end

end
