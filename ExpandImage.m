% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 19/03/2017 00:28.
% Last Revision: 星期日 19/03/2017 00:28.

function [img_large, offset] = ExpandImage(img, exp_ratio, data_opt)

    [img_h, img_w, img_c] = size(img);
    large_h = ceil(exp_ratio * img_h);
    large_w = ceil(exp_ratio * img_w);

    if large_h - img_h <= 4 || large_w - img_w <= 4
        img_large = img;
        offset = [0, 0];
        return;
    end

    img_large = ones(large_h * large_w, img_c);
    if img_c > 1
        if rand > 0.5 && isfield(data_opt, 'gen_channel_mean')
            channel_mean = data_opt.gen_channel_mean;
        else
            channel_mean = mean(reshape(img, img_h * img_w, img_c), 1);
        end

        img_large = bsxfun(@times, img_large, channel_mean);
        img_large = reshape(img_large, large_h, large_w, img_c);
    else
        if rand > 0.5 && isfield(data_opt, 'gen_channel_mean')
            img_large = img_large .* mean(data_opt.gen_channel_mean);
        else
            img_large = img_large .* mean(img(:));
        end
    end

    offset_y_candi = 1 : 2 : (large_h - img_h - 1);
    offset_x_candi = 1 : 2 : (large_w - img_w - 1);
    offset_y = offset_y_candi(randi(numel(offset_y_candi)));
    offset_x = offset_x_candi(randi(numel(offset_x_candi)));
    offset = [offset_y, offset_x];
    img_large(offset(1) + 1 : offset(1) + img_h, ...
                 offset(2) + 1 : offset(2) + img_w, :) = img;

end



