% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 12/03/2017 18:58.
% Last Revision: 星期日 12/03/2017 18:58.

function [bbox, bbox_score, bbox_pose] = ...
    DetectionInference(im, dbox, loc_pred, conf_pred, pose_conf_pred, pose_loc_pred, net_opt)

    im_w = size(im, 2);
    im_h = size(im, 1);

    conf_pred = SoftMax(conf_pred);
    conf_pred = conf_pred(2 : end, :); % remove the negative scores.
    max_conf = max(conf_pred, [], 1);
    [~, sort_idx] = sort(max_conf, 'descend');
    keep_idx = sort_idx(1 : net_opt.test_keep);

    loc_pred = loc_pred(:, keep_idx);
    conf_pred = conf_pred(:, keep_idx);
    pose_conf_pred = pose_conf_pred(:, :, keep_idx);
    pose_loc_pred = pose_loc_pred(:, :, keep_idx);
    dbox = dbox(:, keep_idx);

    % calculate the pre_bbox from the encoded ones.
    pred_bbox = DecodeBBox(dbox, loc_pred, net_opt);
    pred_bbox(1, :) = pred_bbox(1, :) .* im_w;
    pred_bbox(2, :) = pred_bbox(2, :) .* im_h;
    pred_bbox(3, :) = pred_bbox(3, :) .* im_w;
    pred_bbox(4, :) = pred_bbox(4, :) .* im_h;
    pred_bbox = RegularizeBBox(pred_bbox, size(im));

    num_class = size(conf_pred, 1);
    bbox = cell(num_class, 1);
    bbox_score = cell(num_class, 1);
    bbox_pose = cell(num_class, 1);
    for i_class = 1 : num_class
        keep_idx = find(conf_pred(i_class, :) > net_opt.min_score);
        pred_bbox_now = pred_bbox(:, keep_idx);
        conf_pred_now = conf_pred(i_class, keep_idx);
        pose_conf_pred_now = reshape(pose_conf_pred(i_class, :, keep_idx), 32, []);
        pose_loc_pred_now = reshape(pose_loc_pred(i_class, :, keep_idx), 32, []);
        pick_idx = nms([pred_bbox_now', conf_pred_now'], ...
                                        net_opt.nms_overlap);
        pose_pred_now = DecodeQuat(pose_conf_pred_now, pose_loc_pred_now);
        bbox{i_class} = pred_bbox_now(:, pick_idx);
        bbox_score{i_class} = conf_pred_now(pick_idx);
        bbox_pose{i_class} = pose_pred_now(pick_idx, :);
    end

end



