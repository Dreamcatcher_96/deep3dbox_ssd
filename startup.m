% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期四 21/04/2016 22:58.
% Last Revision: 星期二 21/03/2017 17:03.
function startup()

    global process_start;

    process_start = 0;
% --------------------------------------------------------

    addpath('~/work/sina_face/Ori_SSD/');
    addpath(genpath('~/work/sina_face/Ori_SSD/matlab'));
    addpath(genpath('~/work/sina_face/Ori_SSD/examples'));
    addpath(genpath('~/work/sina_face/Ori_SSD/xml_io_tools'));
    addpath(genpath('~/work/sina_face/Ori_SSD/utils'));

    fprintf('SSD Startup Done.\n');

end


