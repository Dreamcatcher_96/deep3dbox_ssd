% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      Friday 13/01/2012 14:02.
% Last Revision: Friday 13/01/2012 14:02.
%QR

function PlotBlockSimple(bbox, linestyle, block_color, show_width, marker)

    if nargin <= 1, linestyle = '-'; end
    if nargin <= 2, block_color = 'blue'; end
    if nargin <= 3, show_width = 2; end
    if nargin <= 4, marker = 'none'; end

    if ~isstruct(bbox)
        bbox_now.x1 = bbox(1);
        bbox_now.y1 = bbox(2);
        bbox_now.x2 = bbox(3);
        bbox_now.y2 = bbox(4);
        bbox = bbox_now;
    end

    % Show the target block.
    % The top edge
    x_plot = [bbox.x1, bbox.x2];
    y_plot = [bbox.y1, bbox.y1];
    plot(x_plot, y_plot, 'LineStyle', linestyle, 'Color', block_color, ...
        'lineWidth', show_width, 'Marker', marker, 'MarkerSize', 2 * show_width, 'MarkerEdgeColor', block_color);
    % The bottom edge
    x_plot = [bbox.x1, bbox.x2];
    y_plot = [bbox.y2, bbox.y2];
    plot(x_plot, y_plot, 'LineStyle', linestyle, 'Color', block_color, ...
        'lineWidth', show_width, 'Marker', marker, 'MarkerSize', 2 * show_width, 'MarkerEdgeColor', block_color);
    % The left edge
    x_plot = [bbox.x1, bbox.x1];
    y_plot = [bbox.y1, bbox.y2];
    plot(x_plot, y_plot, 'LineStyle', linestyle, 'Color', block_color, ...
        'lineWidth', show_width, 'Marker', marker, 'MarkerSize', 2 * show_width, 'MarkerEdgeColor', block_color);
    % The bottom edge
    x_plot = [bbox.x2, bbox.x2];
    y_plot = [bbox.y1, bbox.y2];
    plot(x_plot, y_plot, 'LineStyle', linestyle, 'Color', block_color, ...
        'lineWidth', show_width, 'Marker', marker, 'MarkerSize', 2 * show_width, 'MarkerEdgeColor', block_color);

end





