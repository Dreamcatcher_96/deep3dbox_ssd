% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 11/06/2016 13:52.
% Last Revision: 星期六 11/06/2016 13:52.
%qt

function im_batch = CaffeInputPreProcess(im_cell, ch_mean, net_name, input_siz)

    n_im = length(im_cell);
%     all_siz = cell2mat(all_siz);

    if nargin == 4
        im_siz = [input_siz, input_siz, 3];
    else
        im_siz = size(im_cell{1});
    end

    im_batch = zeros(im_siz(1), im_siz(2), im_siz(3), n_im, 'single');
    for i_im = 1 : n_im
        siz_now = size(im_cell{i_im});
        if any(siz_now ~= im_siz)
            if siz_now(1) ~= siz_now(2)
                regular_img = uint8(ones(max(siz_now(1), siz_now(2)), max(siz_now(1), siz_now(2)), 3));
                regular_img(:, :, 1) = regular_img(:, :, 1)*ch_mean(1);
                regular_img(:, :, 2) = regular_img(:, :, 2)*ch_mean(2);
                regular_img(:, :, 3) = regular_img(:, :, 3)*ch_mean(3);
                regular_img(1:siz_now(1), 1:siz_now(2), :) = im_cell{i_im};
                im_batch(:, :, :, i_im) = imresize(regular_img, im_siz(1 : 2), 'Antialiasing', false);
            else                
                im_batch(:, :, :, i_im) = imresize(im_cell{i_im}, im_siz(1 : 2), 'Antialiasing', false);
            end
        else
            im_batch(:, :, :, i_im) = im_cell{i_im};
        end
    end
    im_batch(:, :, 1, :) = im_batch(:, :, 1, :) - ch_mean(1);
    im_batch(:, :, 2, :) = im_batch(:, :, 2, :) - ch_mean(2);
    im_batch(:, :, 3, :) = im_batch(:, :, 3, :) - ch_mean(3);

    switch net_name
    case 'VGG'
        im_batch = im_batch(:, :, [3, 2, 1], :); % convert from RGB to BGR
    otherwise
    end

    im_batch = permute(im_batch, [2, 1, 3, 4]); % permute width and height

end
