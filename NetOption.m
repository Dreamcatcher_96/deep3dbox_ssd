% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      30/12/2016 10:22.
% Last Revision: 30/12/2016 10:22.
%qt

function net_opt = NetOption(job_name)

    switch job_name
    case {'hanxi_ssd', 'hanxi_ssd_imgaug', 'ori_ssd', 'finetuned_ssd'}
        net_opt = SSDNetOption();
    otherwise
    end

end
