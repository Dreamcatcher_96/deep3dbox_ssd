% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function [loc_loss, conf_loss, pose_loc_loss, pose_conf_loss] = SSDLossForward(pos_dbox_all, ...
            pos_gbox_all, all_loc_preds, all_obj_conf, pose_loc_pred, pose_conf_pred, dbox, net_input_label, ...
                                    neg_dbox_all, num_pos, num_neg, net_opt)

    loc_loss.loss = 0;
    loc_pred_data = [];
    loc_gt_data = [];
    if num_pos >= 1
        for i_im = 1 : net_opt.batch_size
            pos_dbox_idx = pos_dbox_all{i_im};
            matched_gt_idx = pos_gbox_all{i_im};
            if isempty(pos_dbox_idx), continue; end

            % the predictions
            loc_pred = all_loc_preds(:, :, i_im);
            loc_pred_data = cat(1, loc_pred_data, ...
                                loc_pred(:, pos_dbox_idx)');

            % ground truth
            loc_gt = net_input_label{i_im};
            loc_gt = loc_gt(matched_gt_idx, :);

            % real ground truth
            loc_gt_encode = EncodeBBox(...
                    dbox(:, pos_dbox_idx), loc_gt', net_opt)';
            loc_gt_data = cat(1, loc_gt_data, loc_gt_encode);
        end
        % loc forwards
        loc_loss = SmoothL1Forward(loc_pred_data, loc_gt_data);
    end

    num_conf = num_pos + num_neg;
    conf_loss.loss = 0;
    if num_conf > 1
        conf_gt_data = [];
        obj_conf_data = [];

        for i_im = 1 : net_opt.batch_size
            pos_dbox_idx = pos_dbox_all{i_im};
            matched_gt_idx = pos_gbox_all{i_im};
            conf_gt = net_input_label{i_im}(:, 5);

            cur_conf_data = all_obj_conf(:, :, i_im);
            if ~isempty(pos_dbox_idx)
                conf_gt_data = cat(1, conf_gt_data, ...
                                        conf_gt(matched_gt_idx));
                obj_conf_data = cat(2, obj_conf_data, ...
                                            cur_conf_data(:, pos_dbox_idx));

            end

            
            neg_idx = neg_dbox_all{i_im};
            conf_gt_data = cat(1, conf_gt_data, ...
                repmat(net_opt.background_id, numel(neg_idx), 1));
            obj_conf_data = cat(2, obj_conf_data, ...
                                cur_conf_data(:, neg_idx));
        end
        conf_loss = SoftMaxLossForward(obj_conf_data, conf_gt_data);
    end    
    
    if num_pos >= 1
        pose_conf_loss.loss = 0;
        pose_conf_gt_data = [];
        pose_conf_pred_data = [];
    %     obj_conf_data = [];
        for i_im = 1 : net_opt.batch_size
            pos_dbox_idx = pos_dbox_all{i_im};
            if isempty(pos_dbox_idx), continue; end
            matched_gt_idx = pos_gbox_all{i_im};
            pose_conf_pred_data = cat(2, pose_conf_pred_data, reshape(pose_conf_pred(net_input_label{i_im}(:, 5), :, pos_dbox_idx, i_im), 32, []));
            pose_conf_gt = net_input_label{i_im}(:, 6:9);
            pose_conf_gt_data = cat(2, pose_conf_gt_data, pose_conf_gt(matched_gt_idx, :)');
        end
%         pose_conf_pred = pose
        pose_conf_loss = PoseSoftMaxLossForward(pose_conf_pred_data, pose_conf_gt_data);
    end
    
    if num_pos >= 1
        pose_loc_loss.loss = 0;
        pose_loc_pred_data = [];
        pose_loc_gt_data = [];
        for i_im = 1 : net_opt.batch_size
            pos_dbox_idx = pos_dbox_all{i_im};
            if isempty(pos_dbox_idx), continue; end
            matched_gt_idx = pos_gbox_all{i_im};
            pose_conf_gt = net_input_label{i_im}(:, 6:9);
            pose_loc_gt = net_input_label{i_im}(:, 10:end);
            loc_pred = zeros(4, numel(pos_dbox_idx));
            % the predictions
            pose_loc_pred_ = reshape(pose_loc_pred(net_input_label{i_im}(:, 5), 1:8, pos_dbox_idx, i_im), 8, []);
            loc_pred(1, :) = pose_loc_pred_(pose_conf_gt(1) + 1, :);
            pose_loc_pred_ = reshape(pose_loc_pred(net_input_label{i_im}(:, 5), 9:16, pos_dbox_idx, i_im), 8, []);
            loc_pred(2, :) = pose_loc_pred_(pose_conf_gt(2) + 1, :);
            pose_loc_pred_ = reshape(pose_loc_pred(net_input_label{i_im}(:, 5), 17:24, pos_dbox_idx, i_im), 8, []);
            loc_pred(3, :) = pose_loc_pred_(pose_conf_gt(3) + 1, :);
            pose_loc_pred_ = reshape(pose_loc_pred(net_input_label{i_im}(:, 5), 25:32, pos_dbox_idx, i_im), 8, []);
            loc_pred(4, :) = pose_loc_pred_(pose_conf_gt(4) + 1, :);
            pose_loc_pred_data = cat(2, pose_loc_pred_data, loc_pred);
            pose_loc_gt_data = cat(2, pose_loc_gt_data, pose_loc_gt(matched_gt_idx, :)');
        end
        % loc forwards
        pose_loc_loss = AngleL2Forward(pose_loc_pred_data', pose_loc_gt_data');
    end
end


