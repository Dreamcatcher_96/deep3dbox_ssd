% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 19/03/2017 01:35.
% Last Revision: 星期日 19/03/2017 01:35.

function [im_aug, obj_bbox_aug] = ImageAugmentation(im, obj_bbox, data_opt)

    obj_bbox_aug = obj_bbox;
    im_aug = im;

    % color or brightness distortion
    if rand > data_opt.distort_prob
        trans = TransformData();
        im_aug = trans.applyDistort(im);
    end

    % flipping
%     if rand > data_opt.flip_prob
%         im_aug = flip(im_aug, 2);
%         obj_bbox_aug(3, :) = size(im, 2) - obj_bbox(1, :) + 1;
%         obj_bbox_aug(1, :) = size(im, 2) - obj_bbox(3, :) + 1;
%     end

    % add image margin
%     if rand > data_opt.expand_prob
%         expand_ratio = data_opt.min_expand_ratio + ...
%             (data_opt.max_expand_ratio - data_opt.min_expand_ratio) * rand;
%         [im_aug, offset] = ExpandImage(im_aug, expand_ratio, data_opt);
%         obj_bbox_aug([1, 3], :) = obj_bbox_aug([1, 3], :) + offset(2);
%         obj_bbox_aug([2, 4], :) = obj_bbox_aug([2, 4], :) + offset(1);
%     end
% 
%     % for debugging
%     figure(1);
%     subplot(1, 2, 1);imagesc(im);axis equal;hold on;
%     for i_bbox = 1 : size(obj_bbox, 2)
%         PlotBlockSimple(obj_bbox(:, i_bbox), '-', 'g');
%     end
%     subplot(1, 2, 2);imagesc(uint8(im_aug));axis equal;hold on;
%     for i_bbox = 1 : size(obj_bbox_aug, 2)
%         PlotBlockSimple(obj_bbox_aug(:, i_bbox), '-', 'g');
%     end
%     pause(0.1);clf;

end



