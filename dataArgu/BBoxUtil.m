classdef BBoxUtil < handle
   methods(Static)
       function projBBox = projectBBox(srcBBox, bbox)
            if bbox(1) >= srcBBox(3) || bbox(3) <= srcBBox(1) ...
                    || bbox(2) >= srcBBox(4) || bbox(4) <= srcBBox(2)
                projBBox = ones(1, 4) * (-1);
                return ;
            end
            [srcHeight, srcWidth] = BBoxUtil.getBBoxWH(srcBBox);  
            projBBox(1)     = (bbox(1) - srcBBox(1)) / srcWidth;    % xmin
            projBBox(2)     = (bbox(2) - srcBBox(2)) / srcHeight;   % ymin
            projBBox(3)     = (bbox(3) - srcBBox(1)) / srcWidth;    % xmax
            projBBox(4)     = (bbox(4) - srcBBox(2)) / srcHeight;   % ymax
            projBBox        = BBoxUtil.clipBBox(projBBox, [0, 1], [0, 1]);
            if BBoxUtil.bboxSize(projBBox) < 0
                projBBox = ones(1, 4) * (-1);
            end
       end
       
       
       function bbox = clipBBox(bbox, heightRange, widthRange)
            bbox(:, [1, 3]) = max(min(bbox(:, [1, 3]), widthRange(2)), widthRange(1));
            bbox(:, [2, 4]) = max(min(bbox(:, [2, 4]), heightRange(2)), heightRange(1));
       end
       
       function [height, width] = getBBoxWH(bbox, normalized)
            if ~exist('normalized', 'var')
                normalized = true;
            end
            
            if normalized
                height  = bbox(:, 4) - bbox(:, 2);
                width   = bbox(:, 3) - bbox(:, 1);
            else
                height  = bbox(:, 4) - bbox(:, 2) + 1;
                width   = bbox(:, 3) - bbox(:, 1) + 1;                
            end
       end
       
       function siz = bboxSize(bbox)
           [height, width] = BBoxUtil.getBBoxWH(bbox);
           siz  = height .* width;
       end
       
       function bbox = normalizeBBox(height, width, bbox)
            bbox(:, [1, 3]) = bbox(:, [1, 3]) / width;
            bbox(:, [2, 4]) = bbox(:, [2, 4]) / height;
            bbox = BBoxUtil.clipBBox(bbox, [0, 1], [0, 1]);
       end
       
       function bbox = recoverBBox(height, width, bbox)
            bbox(:, [1, 3]) = bbox(:, [1, 3]) * width;
            bbox(:, [2, 4]) = bbox(:, [2, 4]) * height;
            bbox = BBoxUtil.clipBBox(bbox, [1, height], [1, width]);
       end
       
       function locBBox = locateBBox(srcBBox, bbox)
            srcWidth = srcBBox(:, 3) - srcBBox(:, 1);
            srcHeight= srcBBox(:, 4) - srcBBox(:, 2);
            locBBox(:, 1)  = srcBBox(:, 1) + bbox(:, 1) .* srcWidth;
            locBBox(:, 2)  = srcBBox(:, 2) + bbox(:, 2) .* srcHeight;
            locBBox(:, 3)  = srcBBox(:, 1) + bbox(:, 3) .* srcWidth;
            locBBox(:, 4)  = srcBBox(:, 2) + bbox(:, 4) .* srcHeight;
       end
       
       function o = jaccardOverlap(a, b, normalized, usage)
        % Compute the symmetric intersection over union overlap between a set of
        % bounding boxes in a and a single bounding box in b.
        % a  a matrix where each row specifies a bounding box
        % b  a matrix where each row specifies a bounding box
        % normalized    If bbox is not within range [0, 1],        
        %                   normalized == false,otherwise normalized =
        %                   true
        % AUTORIGHTS
        % -------------------------------------------------------
        % Copyright (C) 2011-2012 Ross Girshick
        % Copyright (C) 2008, 2009, 2010 Pedro Felzenszwalb, Ross Girshick
        % 
        % This file is part of the voc-releaseX code
        % (http://people.cs.uchicago.edu/~rbg/latent/)
        % and is available under the terms of an MIT-like license
        % provided in COPYING. Please retain this notice and
        % COPYING if you use this file (or a portion of it) in
        % your project.
        % -------------------------------------------------------
            if ~exist('normalized', 'var')
               normalized = true; 
            end
            
            if ~exist('usage', 'var')
               usage = 'iou'; 
            end

            o = cell(1, size(b, 1));
            mini_abs    = abs(min([a(:); b(:)]));
            a           = a + mini_abs;
            b           = b + mini_abs;
            for i = 1:size(b, 1)
                x1 = max(a(:,1), b(i,1));
                y1 = max(a(:,2), b(i,2));
                x2 = min(a(:,3), b(i,3));
                y2 = min(a(:,4), b(i,4));
                
                if normalized
                    w = x2-x1;
                    h = y2-y1;
                    aarea = (a(:,3)-a(:,1)) .* (a(:,4)-a(:,2));
                    barea = (b(i,3)-b(i,1)) * (b(i,4)-b(i,2));
                else
                    w = x2-x1+1;
                    h = y2-y1+1;
                    aarea = (a(:,3)-a(:,1)+1) .* (a(:,4)-a(:,2)+1);
                    barea = (b(i,3)-b(i,1)+1) * (b(i,4)-b(i,2)+1);
                end
                inter = w.*h;

                if strcmpi(usage, 'iou')
                    % intersection over union overlap
                    o{i} = inter ./ (aarea+barea-inter);
                elseif strcmpi(usage, 'iom')
                    o{i} = inter ./ min(aarea, barea);
                elseif strcmpi(usage, 'iod')
                    o{i} = inter ./ aarea;
                end
                % set invalid entries to 0 overlap
                o{i}(w <= 0) = 0;
                o{i}(h <= 0) = 0;
            end
            o = cell2mat(o);
       end
       
       function bbox = updateBBoxByResizePolicy(resizeParam, oldHeight, oldWidth, bbox)
            newHeight = resizeParam.height;
            newWidth  = resizeParam.width;
            origAspect= oldWidth / oldHeight;
            newAspect = newWidth / newHeight;
            
            xMin    = bbox(:, 1) * oldWidth;
            xMax    = bbox(:, 3) * oldWidth;
            yMin    = bbox(:, 2) * oldHeight;
            yMax    = bbox(:, 4) * oldHeight;
            if strcmp(upper(resizeParam.resize_mode), 'WARP')
                xMin    = max(xMin * newWidth / oldWidth, 0);
                xMax    = min(xMax * newWidth / oldWidth, newWidth);
                yMin    = max(yMin * newHeight/ oldHeight, 0);
                yMax    = min(yMax * newHeight/ oldHeight, newHeight);      
            elseif strcmp(upper(resizeParam.resize_mode), 'FIT_SMALL_SIZE')
                if origAspect < newAspect
                    newHeight = newWidth / origAspect;
                else
                    newWidth = origAspect * newHeight;
                end
                xMin = max(xMin * newWidth / oldWidth, 0);
                xMax = min(xMax * newWidth / oldWidth, newWidth);
                yMin = max(yMin * newHeight / oldHeight, 0);
                yMax = min(yMax * newHeight / oldHeight, newHeight);
            elseif strcmp(upper(resizeParam.resize_mode), 'FIT_LARGE_SIZE_AND_PAD')
                  if origAspect > newAspect
                        padding = (newHeight - newWidth / origAspect) / 2;
                        xMin    = max(xMin * newWidth / oldWidth, 0);
                        xMax    = min(xMax * newWidth / oldWidth, newWidth);
                        yMin    = yMin * (newHeight - 2 * padding) / oldHeight;
                        yMin    = padding + max(yMin, 0);
                        yMax    = yMax * (newHeight - 2 * padding) / oldHeight;
                        yMax    = padding + min(newHeight, yMax);
                  else
                        padding = (newWidth - newHeight * origAspect) / 2;
                        xMin    = xMin * (newWidth - 2 * padding) / oldWidth;
                        xMin    = padding + max(xMin, 0);
                        xMax    = xMax * (newWidth - 2 * padding) / oldWidth;
                        xMax    = padding + min(newWidth, xMax);
                        xMax    = min(xMax * newWidth / oldWidth, newWidth);     
                        yMin    = max(yMin * newHeight / oldHeight, 0);
                        yMax    = min(yMax * newHeight / oldHeight , newHeight);
                  end
            else
               error('Unknown resize mode.'); 
            end
            bbox = BBoxUtil.normalizeBBox(newHeight, newWidth, [xMin, yMin, xMax, yMax]);
       end
       
       function isMeet = meetEmitConstraint(srcBBox, bbox, emitConstraint)
            emitType = emitConstraint.emit_type;
            isMeet   = false;
            if strcmpi(emitType, 'CENTER')
                xCenter = (bbox(:, 1) + bbox(:, 3)) / 2;
                yCenter = (bbox(:, 2) + bbox(:, 4)) / 2;
                if xCenter >= srcBBox(1) & xCenter <= srcBBox(3) ...
                        & yCenter >= srcBBox(2) & yCenter <= srcBBox(4)
                    isMeet = true;
                else
                    isMeet = false;
                end
            elseif strcmpi(emitType, 'MIN_OVERLAP')
                error('only support CENTER mode.');
            else
                error('Unknown emit type.');
            end
       end
       
       
       function boxes = xy2wh(boxes)
            boxes(:, 3) = boxes(:, 3) - boxes(:, 1);
            boxes(:, 4) = boxes(:, 4) - boxes(:, 2);            
       end
       
       function boxes = wh2xy(boxes)
            boxes(:, 3) = boxes(:, 3) + boxes(:, 1);
            boxes(:, 4) = boxes(:, 4) + boxes(:, 2);             
       end
       
       function annoTrans = transformAnno(anno)
            annoTrans.boxes     = cell2mat(cellfun(@(x) single(x), {anno(:).boxes}', 'UniformOutput', false));
            annoTrans.class     = cell2mat({anno(:).class}');
            annoTrans.difficult = cell2mat({anno(:).difficult}');
            annoTrans.gtNum     = cellfun(@(x) length(x), {anno(:).class}');
            itemId              = [];
            for ii = 1 : length(annoTrans.gtNum)
               itemId = cat(1, itemId, ones(annoTrans.gtNum(ii), 1) * ii);
            end
            annoTrans.itemId = itemId;
       end
       
       
       function [regressionLabel] = encodeBBoxes(priorbox, gt)
           priorWidth   = priorbox(:, 3) - priorbox(:, 1);
           assert(all(priorWidth > 0));
           priorHeight  = priorbox(:, 4) - priorbox(:, 2);
           assert(all(priorHeight > 0));
           
           priorCenterX = (priorbox(:, 1) + priorbox(:, 3)) / 2;
           priorCenterY = (priorbox(:, 2) + priorbox(:, 4)) / 2;
           
           gtWidth      = gt(:, 3) - gt(:, 1);
           assert(all(gtWidth > 0));
           gtHeight     = gt(:, 4) - gt(:, 2);
           assert(all(gtHeight > 0));
           gtCenterX    = (gt(:, 1) + gt(:, 3)) / 2;
           gtCenterY    = (gt(:, 2) + gt(:, 4)) / 2;
           
           regressionLabel(:, 1) = (gtCenterX - priorCenterX) ./ priorWidth  ./ 0.1;
           regressionLabel(:, 2) = (gtCenterY - priorCenterY) ./ priorHeight ./ 0.1;
           regressionLabel(:, 3) = log(gtWidth ./ priorWidth) ./ 0.2;
           regressionLabel(:, 4) = log(gtHeight ./ priorHeight) ./ 0.2;       
       end
       
       
       function bboxes = decodeBBoxes(priorboxes, param)
            priorboxes_width    = priorboxes(:, 3) - priorboxes(:, 1);
            priorboxes_height   = priorboxes(:, 4) - priorboxes(:, 2);
            priorboxes_center_x = (priorboxes(:, 1) + priorboxes(:, 3)) / 2;
            priorboxes_center_y = (priorboxes(:, 2) + priorboxes(:, 4)) / 2;

            decode_bboxes_center_x  = param(:, 1) .* priorboxes_width * 0.1 + priorboxes_center_x;
            decode_bboxes_center_y  = param(:, 2) .* priorboxes_height* 0.1 + priorboxes_center_y;
            decode_bboxes_width            = exp(0.2 * param(:, 3)) .* priorboxes_width;
            decode_bboxes_heigh            = exp(0.2 * param(:, 4)) .* priorboxes_height;

            bboxes                  = [decode_bboxes_center_x - decode_bboxes_width/2, ...
                                       decode_bboxes_center_y - decode_bboxes_heigh/2, ...
                                       decode_bboxes_center_x + decode_bboxes_width/2, ...
                                       decode_bboxes_center_y + decode_bboxes_heigh/2];           
       end
   end
end