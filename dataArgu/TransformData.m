classdef TransformData < handle
   properties(Access = private)
       transformationParam
   end

   methods
       function obj = TransformData()
           obj.transformationParam = transform_param();
       end

       function img = applyDistort(obj, img)
           if ~obj.hasDistortParam()
               return ;
           end
           prob     = rng_uniform(1, 0, 1);
           param    = obj.getDistortParam();

           if prob > 0.5
                % Do random brightness distortion. 亮度
                img =   obj.randomBrightness(img, param.brightness_prob, ...
                                                 param.brightness_delta);
                % Do random contrast distortion. 对比度
                img =   obj.randomContrast(img, param.contrast_prob, ...
                                               param.contrast_lower, param.contrast_upper);
                % Do random saturation distortion. 饱和度
                img = obj.randomSaturation(img, param.saturation_prob, ...
                                                param.saturation_lower, param.saturation_upper);
                % Do random hue distortion. 色调
                img = obj.randomHue(img, param.hue_prob, param.hue_delta);

                % Do random reordering of the channels. 随机交换颜色通道
                img = obj.randomOrderChannels(img, param.random_order_prob);
           else
                % Do random brightness distortion.
                img =   obj.randomBrightness(img, param.brightness_prob, ...
                                                 param.brightness_delta);
                % Do random saturation distortion.
                img = obj.randomSaturation(img, param.saturation_prob, ...
                                                param.saturation_lower, param.saturation_upper);
                % Do random hue distortion.
                img = obj.randomHue(img, param.hue_prob, param.hue_delta);

                % Do random contrast distortion.
                img =   obj.randomContrast(img, param.contrast_prob, ...
                                               param.contrast_lower, param.contrast_upper);
                % Do random reordering of the channels.
                img = obj.randomOrderChannels(img, param.random_order_prob);
           end
       end


       function [img, bboxes] = expandImage(obj, img, bboxes)
           if ~obj.hasExpandParam()
              return ;
           end

           param = obj.getExpandParam();
           expandProb = param.prob;
           prob  = rng_uniform(1, 0, 1);
           if prob > expandProb
              return ;
           end

           maxExpandRatio = param.max_expand_ratio;
           if abs(maxExpandRatio - 1) < 1e-2
              return ;
           end
           expandRatio = rng_uniform(1, 1, maxExpandRatio);
           if abs(expandRatio - 1) < 1e-1
              return ;
           end
           [expandImg, expandBBox] = obj.doExpand(img, expandRatio, obj.getMeanValue());

           % Transform the annotation according to crop_bbox.
           if exist('bboxes', 'var')
                bboxes = obj.transformAnnotation(img, false, expandBBox, false, bboxes);
           end
           img  = expandImg;
       end

       function [resizedImg, BBoxes] = applyResize(obj, img, BBoxes)
           resizeParam  = obj.getResizeParam();
           newHeight    = resizeParam.height;
           newWidth     = resizeParam.width;
           numInterpMode= length(resizeParam.interp_mode);
%            padMode      = resizeParam.pad_mode;
           if numInterpMode > 0
              probs     = cumsum(ones(1, numInterpMode) * (1/numInterpMode));
              val       = rng_uniform(1, 0, probs(end));
              valIdx    = find(val > probs == 0);
              valIdx    = valIdx(1);
              interpMode= resizeParam.interp_mode{valIdx};
           else
               error('interp mode must not be empty');
           end

%            [height, width, channels] = size(img);
%            padVal       = zeros(1, channels);
%            if isfield('resizeParam', 'pad_value')
%                 numPadValue  = length(resizeParam.pad_value);
%                 assert(numPadValue == 1 || numPadValue == channels, ...
%                 	sprintf('Specify either 1 pad_value or as many as channels: %d', channels));
%                 if numPadValue == 1
%                    padValues    = repmat(resizeParam.pad_value, 1, channels);
%                 end
%                 padVal  = padValues;
%            end

           resizeMode   = resizeParam.resize_mode;
           if strcmpi(resizeMode, 'WARP')
               resizedImg = imresize(img, [newHeight, newWidth], interpMode, 'AntiAliasing',false);
           elseif strcmpi(resizeMode, 'FIT_SMALL_SIZE')
               error('only support WARP mode');
           elseif strcmpi(resizeMode, 'FIT_LARGE_SIZE_AND_PAD')
               error('only support WARP mode');
           end

           if obj.transformationParam.mirror
               doMirror     = false;
               if rng_uniform(1, 0, 1) > 0.5
                    resizedImg = fliplr(resizedImg);
                    doMirror = true;
               end
           end
           doResize     = true;
           cropBBox     = [0, 0, 1, 1];
           BBoxes = obj.transformAnnotation(img, doResize, cropBBox, doMirror, BBoxes);
       end

       function BBoxes = transformAnnotation(obj, img, doResize, cropBBox, doMirror, BBoxes)
            [height, width, ~] = size(img);
            annoNum     = size(BBoxes, 1);
            for ii = 1 : annoNum
                bbox    = BBoxes(ii, :);
                if doResize && obj.hasResizeParam()
                    assert(height > 0, 'image height must be non-negative.');
                    assert(width > 0, 'image width must be non-negative.');
                    bbox = BBoxUtil.updateBBoxByResizePolicy(obj.getResizeParam, height, width, BBoxes(ii, :));
                end
                if obj.hasEmitConstraint() && ~BBoxUtil.meetEmitConstraint(cropBBox, bbox, obj.getEmitConstraint())
                    BBoxes(ii, :) = 0;
                    continue;
                end
                BBoxes(ii, :) = BBoxUtil.projectBBox(cropBBox, bbox);
                if all(BBoxes(ii, :) ~= -1)
                    if doMirror
                        temp = BBoxes(ii, 1);
                        BBoxes(ii, 1) = 1 - BBoxes(ii, 3);
                        BBoxes(ii, 3) = 1 - temp;
                    end
                end
            end
       end
   end

   methods(Access = private)
       function hasParam = hasDistortParam(obj)
            hasParam = isfield(obj.transformationParam, 'distort_param');
       end

       function param = getDistortParam(obj)
            param =  obj.transformationParam.distort_param;
       end

       function hasParam = hasExpandParam(obj)
           hasParam = isfield(obj.transformationParam, 'expand_param');
       end

       function param = getExpandParam(obj)
            param =  obj.transformationParam.expand_param;
       end

       function hasParam = hasResizeParam(obj)
            hasParam = isfield(obj.transformationParam, 'resize_param');
       end

       function param = getResizeParam(obj)
            param =  obj.transformationParam.resize_param;
       end

       function meanValue = getMeanValue(obj)
            if isfield(obj.transformationParam, 'mean_value')
                meanValue = obj.transformationParam.mean_value;
            else
                meanValue = zeros(1, 3);
            end
       end

       function hasEmit = hasEmitConstraint(obj)
           hasEmit = isfield(obj.transformationParam, 'emit_constraint');
       end

       function emitConstraint = getEmitConstraint(obj)
           emitConstraint = obj.transformationParam.emit_constraint;
       end

       function img = randomBrightness(obj, img, ...
                                        brightness_prob, brightness_delta)
            prob     = rng_uniform(1, 0, 1);
            if prob < brightness_prob
                assert(brightness_prob >= 0, 'brightness_delta must be non-negative.');
                delta   = rng_uniform(1, -brightness_delta, brightness_delta);
                img     = bsxfun(@plus, img, delta);
            end
       end

       function img = randomContrast(obj, img, contrast_prob, ...
                                                contrast_lower, contrast_upper)
           prob     = rng_uniform(1, 0, 1);
           if prob < contrast_prob
                assert(contrast_lower <= contrast_upper, 'contrast upper must be >= lower.');
                assert(contrast_lower >= 0, 'contrast lower must be non-negative.');

                delta = rng_uniform(1, contrast_lower, contrast_upper);
                if abs(delta - 1) > 1e-3
                    img   = bsxfun(@times, img, delta);
                end
           end
       end

       function img = randomSaturation(obj, img, saturation_prob, ...
                                                    saturation_lower, saturation_upper)
            prob    = rng_uniform(1, 0, 1);
            if prob < saturation_prob
                assert(saturation_lower <= saturation_upper, 'saturation upper must be >= lower.');
                assert(saturation_lower >= 0, 'saturation lower must be non-negative.');

                delta = rng_uniform(1, saturation_lower, saturation_upper);
                if abs(delta - 1) > 1e-3
%                     hsvImg = OpenCVFunction.rgb2hsv(img);
                    hsvImg = rgb2hsv(img);
                    hsvImg(:, :, 2) = bsxfun(@times, hsvImg(:, :, 2), delta);
                    hsvImg(:, :, 2) = min(1, max(0, hsvImg(:, :, 2)));
%                     img = uint8(OpenCVFunction.hsv2rgb(hsvImg) * 255);
                    img = uint8(hsv2rgb(hsvImg) * 255);
                end
            end
       end

       function img = randomHue(obj, img, hue_prob, hue_delta)
           prob    = rng_uniform(1, 0, 1);
           if prob < hue_prob
                assert(hue_delta >= 0, 'hue_delta must be non-negative.');
                delta = rng_uniform(1, -hue_delta, hue_delta);
                if (abs(delta) > 0)
%                     hsvImg = OpenCVFunction.rgb2hsv(img);
                    hsvImg = rgb2hsv(img);
                    hsvImg(:, :, 1) = bsxfun(@plus, hsvImg(:, :, 1), delta);
                    hsvImg(:, :, 1) = min(1, max(0, hsvImg(:, :, 1)));
%                     img = uint8(OpenCVFunction.hsv2rgb(hsvImg) * 255);
                    img = uint8(hsv2rgb(hsvImg) * 255);
                end
           end
       end

       function img = randomOrderChannels(obj, img, random_order_prob)
           prob    = rng_uniform(1, 0, 1);
           if prob < random_order_prob
              nChan = size(img, 3);
              img = img(:, :, [randperm(nChan, nChan)]);
           end
       end

       function [expandImg, expandBBox] = doExpand(obj, img, expandRatio, ...
                                                        meanValue)
           [imgHeight, imgWidth, imgChannels] = size(img);
           height   = int16(imgHeight * expandRatio);
           width    = int16(imgWidth * expandRatio);
           hOff     = floor(rng_uniform(1, 1, height - imgHeight));
           wOff     = floor(rng_uniform(1, 1, width - imgWidth));
           % expandBBox = [xmin, ymin, xmax, ymax]
           expandBBox = [-double(wOff) / imgWidth, ...
                         -double(hOff) / imgHeight, ...
                         double(width - wOff) / imgWidth, ...
                         double(height- hOff) / imgHeight];
           expandImg = zeros(height, width, imgChannels);
           if exist('meanValue', 'var')
               meanValueSize = length(meanValue);
               assert(meanValueSize == 1 | meanValueSize == imgChannels, ...
                    sprintf('Specify either 1 mean_value or as many as channels: %d', imgChannels));
               if meanValueSize == 1
                  meanValue = repmat(meanValue, 1, imgChannels);
               end
               expandImg(:, :, 1) = meanValue(1);
               expandImg(:, :, 2) = meanValue(2);
               expandImg(:, :, 3) = meanValue(3);

%                expandImg = cat(3, ones(height, width) * meanValue(1), ...
%                                   ones(height, width) * meanValue(2), ...
%                                   ones(height, width) * meanValue(3));
%
               expandImg(hOff : hOff + imgHeight - 1, ...
                         wOff : wOff + imgWidth - 1, :) ...
                         = img;
           end
           if exist('expandImg', 'var')
                expandImg = uint8(expandImg);
           else
                expandImg = uint8(zeros(height, width, imgChannels));
           end
       end
   end
end
