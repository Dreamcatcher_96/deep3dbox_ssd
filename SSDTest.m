% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期五 31/03/2017 16:16.
% Last Revision: 星期五 31/03/2017 16:16.

function [class_ap, class_pose_acc, class_prec, class_rec] = SSDTest(dataset, test_net, all_class_name, net_opt)

    num_tst = numel(dataset);
    num_class = numel(all_class_name);

    n_batch = ceil(num_tst ./ net_opt.batch_size);
    u = n_batch * net_opt.batch_size - num_tst;
    batch_idx_all = reshape([1 : num_tst, randi(num_tst, 1, u)], ...
                                    net_opt.batch_size, []);
    bbox_pred = cell(num_class, num_tst);
    score_pred = cell(num_class, num_tst);
    pose_pred = cell(num_class, num_tst);
    aboxes = cell(num_class, num_tst);
    ground_truth = cell(num_class, num_tst);
    pose_gt = cell(num_class, num_tst);
    obj_difficulty = cell(num_class, num_tst);
    been_tested = false(1, num_tst);
    for i_tst = 1 : size(batch_idx_all, 2)

        fprintf('Hanxi SSD test start, batch-%d, %d images, ', ...
                                    i_tst, size(batch_idx_all, 1));
        tst_start = tic;

        % get the input of the ssd
        batch_idx = batch_idx_all(:, i_tst);
        batch_dataset = dataset(batch_idx);
        [tst_img, tst_obj_bbox, tst_obj_class, tst_obj_quat, tst_obj_difficulty] = ...
                                            SSDTestImage(batch_dataset);
        net_input_data = GetCaffeInput(tst_img, ...
                    tst_obj_bbox, tst_obj_class, tst_obj_quat, tst_obj_quat, net_opt);

        % forward
        FlexibleForward(net_input_data, test_net);

        % detection inference
        mbox_loc = test_net.blobs('mbox_loc').get_data();
        mbox_conf = test_net.blobs('mbox_conf').get_data();
        multibin_loc = test_net.blobs('mbox_pose_loc').get_data();
        multibin_conf = test_net.blobs('mbox_pose_conf').get_data();
        loc_pred = reshape(mbox_loc, 4, [], net_opt.batch_size);
        conf_pred = reshape(mbox_conf, num_class + 1, [], net_opt.batch_size);
        pose_loc_pred = reshape(multibin_loc, num_class, 32, [], net_opt.batch_size);
        pose_conf_pred = reshape(multibin_conf, num_class, 32, [], net_opt.batch_size);

        % detection inference
        bbox_pred_batch = cell(num_class, length(tst_img));
        score_pred_batch = cell(num_class, length(tst_img));
        pose_pred_batch = cell(num_class, length(tst_img));
        for i_img = 1 : length(tst_img)
            loc_pred_now = loc_pred(:, :, i_img);
            conf_pred_now = conf_pred(:, :, i_img);
            pose_conf_pred_now = pose_conf_pred(:, :, :, i_img);
            pose_loc_pred_now = pose_loc_pred(:, :, :, i_img);
            [bbox_pred_batch(:, i_img), score_pred_batch(:, i_img), pose_pred_batch(:, i_img)] = ...
                DetectionInference(tst_img{i_img}, net_opt.dbox, loc_pred_now, ...
                                                            conf_pred_now, pose_conf_pred_now, pose_loc_pred_now, net_opt);
        end

        fprintf(' ... done, took %2.3f sec.\n', toc(tst_start));

        % save detection results
        bbox_pred(:, batch_idx) = bbox_pred_batch;
        score_pred(:, batch_idx) = score_pred_batch;
        pose_pred(:, batch_idx) = pose_pred_batch;
        for i_class = 1 : num_class
            aboxes(i_class, batch_idx) = cellfun(@(x,y,z) [x', y', z], ...
                bbox_pred_batch(i_class, :), score_pred_batch(i_class, :), pose_pred_batch(i_class, :), 'UniformOutput', false);
            this_class = cellfun(@(x) x == i_class, ...
                        tst_obj_class, 'UniformOutput', false);
            ground_truth(i_class, batch_idx) = ...
                cellfun(@(x,y) x(:, y)', tst_obj_bbox, this_class, 'UniformOutput', false);            
            pose_gt(i_class, batch_idx) = ...
                cellfun(@(x,y) x(:, y)', tst_obj_quat, this_class, 'UniformOutput', false);
            obj_difficulty(i_class, batch_idx) = cellfun(@(x,y) x(y), ...
                        tst_obj_difficulty, this_class, 'UniformOutput', false);
        end
        been_tested(batch_idx) = true;

    end

    aboxes(:, ~been_tested) = [];
    ground_truth(:, ~been_tested) = [];
    obj_difficulty(:, ~been_tested) = [];
    [class_ap, class_pose_acc, class_prec, class_rec] = DetectionResEval(aboxes, ground_truth, pose_gt, ...
            obj_difficulty, all_class_name, all_class_name, net_opt.nms_overlap);

end



