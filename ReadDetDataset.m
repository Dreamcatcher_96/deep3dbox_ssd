% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 19/03/2017 11:14.
% Last Revision: 星期日 19/03/2017 11:14.

function [im, obj_bbox, obj_class, obj_conf, obj_loc, obj_difficulty, obj_quat] = ReadDetDataset(im_data)

%     im_name = [data_opt.image_root, '/', im_data.filename];
    im_name = im_data.filename;
    im = imread(im_name);
    obj = im_data.object;
%     im_siz = size(im);

    num_obj = numel(obj);
    obj_bbox = zeros(4, num_obj);
    obj_class = zeros(1, num_obj);
    obj_conf = zeros(4, num_obj);
    obj_difficulty = zeros(1, num_obj);
    obj_loc = zeros(4, num_obj);
    obj_quat= zeros(4, num_obj);
    for i_bbox = 1 : num_obj
        bbox_now = [min(obj(i_bbox).bndbox(:,1)); min(obj(i_bbox).bndbox(:,2)); max(obj(i_bbox).bndbox(:,1)); max(obj(i_bbox).bndbox(:,2))];
        obj_bbox(:, i_bbox) = bbox_now';
        obj_class(i_bbox) = obj(i_bbox).class_id;
        obj_difficulty(i_bbox) = obj(i_bbox).difficult;
        R = obj(i_bbox).RT(1:3, 1:3);
        quat = rotm2quat(R);
        obj_quat(:, i_bbox) = quat;
        obj_conf(:, i_bbox) = floor((quat+1)*100/(200/8));
        obj_loc(:, i_bbox) = (quat+1)' - 200 / 8 .* obj_conf(:, i_bbox) ./ 100;
    end

end



