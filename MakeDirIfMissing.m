% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期五 30/12/2016 10:11.
% Last Revision: 星期五 30/12/2016 10:11.
%qt

function flag_mk = MakeDirIfMissing(folder_name)

    if ~exist(folder_name, 'dir')
        mkdir(folder_name);
        flag_mk = 1;
    else
        flag_mk = 0;
    end

end



