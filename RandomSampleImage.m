% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期一 09/01/2017 21:22.
% Last Revision: 星期一 09/01/2017 21:22.
%qt

function subimg_bbox = RandomSampleImage(sampler, object_bboxes)

    % random parameters of crop bbox
    scale = sampler.min_scale + rand(1, sampler.max_trials) .* ...
                                (sampler.max_scale - sampler.min_scale);
    min_ar = max(sampler.min_aspect_ratio, scale.^2);
    max_ar = min(sampler.max_aspect_ratio, scale.^0.5);
    ar_interval = max_ar - min_ar;
    aspect_ratio = min_ar + rand(size(ar_interval)) .* ar_interval;

    % bbox dimension
    bbox_width = scale .* sqrt(aspect_ratio);
    bbox_height = scale ./ sqrt(aspect_ratio);

    % top left coordinates
    x_start = rand(size(bbox_width)) .* (1 - bbox_width);
    y_start = rand(size(bbox_height)) .* (1 - bbox_height);

    % bbox coordinates (xmin, ymin, xmax, ymax)
    bbox_candi = [x_start(:)'; y_start(:)'; ...
        x_start(:)' + bbox_width(:)'; y_start(:)' + bbox_height(:)'];

    % overlap between the crop bbox candidates and the object bbox
    O = CalcBoxOverlap(bbox_candi, object_bboxes, 'int_uni');

    % the qualified crop bbox
    if isfield(sampler.sample_constraint, 'min_jaccard_overlap')
        is_qualified = any(O > ...
            sampler.sample_constraint.min_jaccard_overlap, 2);
    elseif isfield(sampler.sample_constraint, 'max_jaccard_overlap')
        is_qualified = any(O < ...
            sampler.sample_constraint.max_jaccard_overlap, 2);
    end

    if sum(is_qualified) == 0
        subimg_bbox = [];
    else
        bbox_candi = bbox_candi(:, is_qualified);
        subimg_bbox = bbox_candi(:, randi(size(bbox_candi, 2)));
    end

end



