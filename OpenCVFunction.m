classdef OpenCVFunction < handle
   methods(Static)
       function hsv = rgb2hsv(rgb)
            rgb     = double(rgb);
            rgb     = rgb / 255;
            R       = rgb(:, :, 1);     G       = rgb(:, :, 2);     B       = rgb(:, :, 3);
            H       = zeros(size(R));   S       = zeros(size(R));   V       = zeros(size(R));

            varMax  = max(rgb, [], 3);
            varMin  = min(rgb, [], 3);
            delMax  = varMax - varMin;

            V       = varMax;
            valIdx  = V ~= 0;
            S(valIdx) = (V(valIdx) - varMin(valIdx)) ./ V(valIdx);

            MaxEqMin    = find(varMax == varMin);
            VeqR        = setdiff(find(V == R), MaxEqMin);
            VeqG        = setdiff(find(V == G), MaxEqMin);
            VeqB        = setdiff(find(V == B), MaxEqMin);


            H(VeqR) = 60 * (G(VeqR) - B(VeqR)) ./ delMax(VeqR);
            H(intersect(VeqR, find(G < B))) = H(intersect(VeqR, find(G < B))) + 360;
            H(VeqG) = 60 * (B(VeqG) - R(VeqG)) ./ delMax(VeqG) + 120;
            H(VeqB) = 60 * (R(VeqB) - G(VeqB)) ./ delMax(VeqB) + 240;
            H(MaxEqMin) = 0;

            V   = V * 255;
            S   = S * 255;
            H   = H / 2;
            hsv = cat(3, H, S, V);           
       end
       
       
       function rgb = hsv2rgb(hsv)
            H       = hsv(:, :, 1) * 2;     
            S       = hsv(:, :, 2) / 255;     
            V       = hsv(:, :, 3) / 255;

            R       = zeros(size(H));
            G       = zeros(size(H));
            B       = zeros(size(H));

            I       = mod(floor(H / 60), 6);
            F       = H / 60 - I;
            P       = V .* (1 - S);
            Q       = V .* (1 - S .* F);
            T       = V .* (1 - S .* (1 - F));
            SeqZero = find(S == 0);
            IeqZero = setdiff(find(I == 0), SeqZero);
            IeqOne  = setdiff(find(I == 1), SeqZero);
            IeqTwo  = setdiff(find(I == 2), SeqZero);
            IeqThr  = setdiff(find(I == 3), SeqZero);
            IeqFour = setdiff(find(I == 4), SeqZero);
            IeqFive = setdiff(find(I == 5), SeqZero);

            R(IeqZero) = V(IeqZero);
            G(IeqZero) = T(IeqZero);
            B(IeqZero) = P(IeqZero);

            R(IeqOne) = Q(IeqOne);
            G(IeqOne) = V(IeqOne);
            B(IeqOne) = P(IeqOne);

            R(IeqTwo) = P(IeqTwo);
            G(IeqTwo) = V(IeqTwo);
            B(IeqTwo) = T(IeqTwo);

            R(IeqThr) = P(IeqThr);
            G(IeqThr) = Q(IeqThr);
            B(IeqThr) = V(IeqThr);

            R(IeqFour) = T(IeqFour);
            G(IeqFour) = P(IeqFour);
            B(IeqFour) = V(IeqFour);

            R(IeqFive) = V(IeqFive);
            G(IeqFive) = P(IeqFive);
            B(IeqFive) = Q(IeqFive);

            R(SeqZero) = V(SeqZero);
            G(SeqZero) = V(SeqZero);
            B(SeqZero) = V(SeqZero);

            rgb     = cat(3, R, G, B);           
       end
   end
end