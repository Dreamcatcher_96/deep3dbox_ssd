% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:35.
% Last Revision: 星期二 24/01/2017 23:35.
%qt

function SSDLossBackward(caffe_solver, loc_loss, conf_loss, pose_loc_loss, pose_conf_loss, pos_dbox_all, ...
                                neg_dbox_all, net_input_label, num_pos, net_opt)

    loc_diff = zeros(size(caffe_solver.net.blobs('mbox_loc').get_diff()));
    caffe_solver.net.blobs('mbox_loc').set_diff(loc_diff);

    if num_pos > 0
       loc_loss = SmoothL1Backward(loc_loss);
       loss_weight = 1 / num_pos;
       loc_pred_diff = loc_loss.loc_pred_diff * loss_weight;
    end

    loc_diff = reshape(loc_diff, 4, [], net_opt.batch_size);
    for i_im = 1 : net_opt.batch_size
        % the index of the updating neurons.
        pos_dbox_idx = pos_dbox_all{i_im};
        if isempty(pos_dbox_idx)
           continue;
        end
        loc_diff(:, pos_dbox_idx, i_im) = loc_pred_diff(1:numel(pos_dbox_idx), :)';
        loc_pred_diff(1:numel(pos_dbox_idx), :) = [];
    end
    loc_diff = reshape(loc_diff, [], net_opt.batch_size);
    caffe_solver.net.blobs('mbox_loc').set_diff(loc_diff);

    % Back propagate on confidence prediction.
    conf_diff = zeros(size(caffe_solver.net.blobs('mbox_conf').get_diff()));
    caffe_solver.net.blobs('mbox_conf').set_diff(conf_diff);


    conf_loss = SoftMaxLossBackward(conf_loss);
    obj_conf_diff = conf_loss.pred_diff ./ size(conf_loss.pred_diff, 2);

    conf_diff = reshape(conf_diff, net_opt.num_classes, ...
                                                [], net_opt.batch_size);

    for i_im = 1 : net_opt.batch_size

        pos_dbox_idx = pos_dbox_all{i_im};
        if ~isempty(pos_dbox_idx)
            conf_diff(:, pos_dbox_idx, i_im) = obj_conf_diff(:, 1:numel(pos_dbox_idx));
            obj_conf_diff(:, 1:numel(pos_dbox_idx)) = [];
        end

        

        neg_idx = neg_dbox_all{i_im};
        if isempty(neg_idx)
           continue;
        end

        conf_diff(:, neg_idx, i_im) = obj_conf_diff(:, 1:numel(neg_idx));
        obj_conf_diff(:, 1:numel(neg_idx)) = [];
    end
    conf_diff = reshape(conf_diff, [], net_opt.batch_size);
    caffe_solver.net.blobs('mbox_conf').set_diff(conf_diff);
    
    loc_diff = zeros(size(caffe_solver.net.blobs('mbox_pose_loc').get_diff()));
    caffe_solver.net.blobs('mbox_pose_loc').set_diff(loc_diff);
    
    pose_loc_loss = AngleL2Backward(pose_loc_loss);
    loss_weight = 1 / net_opt.batch_size;
    loc_pred_diff = pose_loc_loss.loc_pred_diff * loss_weight * 0.7;
    
    loc_diff = reshape(loc_diff, net_opt.num_classes-1, 32, [], net_opt.batch_size);
    loc_diff = reshape(loc_diff, net_opt.num_classes-1, [], 4, size(loc_diff, 3), net_opt.batch_size);
    for i_im = 1 : net_opt.batch_size
        pos_dbox_idx = pos_dbox_all{i_im};
        if isempty(pos_dbox_idx)
           continue;
        end
        pose_conf_gt = net_input_label{i_im}(:, 6:9);
        loc_diff(net_input_label{i_im}(:, 5), pose_conf_gt(1)+1, 1, pos_dbox_idx, i_im) = loc_pred_diff(1:numel(pos_dbox_idx), 1);
        loc_diff(net_input_label{i_im}(:, 5), pose_conf_gt(2)+1, 2, pos_dbox_idx, i_im) = loc_pred_diff(1:numel(pos_dbox_idx), 2);
        loc_diff(net_input_label{i_im}(:, 5), pose_conf_gt(3)+1, 3, pos_dbox_idx, i_im) = loc_pred_diff(1:numel(pos_dbox_idx), 3);
        loc_diff(net_input_label{i_im}(:, 5), pose_conf_gt(4)+1, 4, pos_dbox_idx, i_im) = loc_pred_diff(1:numel(pos_dbox_idx), 4);
        loc_pred_diff(1:numel(pos_dbox_idx), :) = [];
    end
    loc_diff = reshape(loc_diff, [], net_opt.batch_size);
    caffe_solver.net.blobs('mbox_pose_loc').set_diff(loc_diff);

    % Back propagate on confidence prediction.
    conf_diff = zeros(size(caffe_solver.net.blobs('mbox_pose_conf').get_diff()));
    caffe_solver.net.blobs('mbox_pose_conf').set_diff(conf_diff);
    conf_diff = reshape(conf_diff, net_opt.num_classes-1, 32, [], net_opt.batch_size);
    pose_conf_loss = PoseSoftMaxLossBackward(pose_conf_loss);
    obj_conf_diff = zeros(32, size(pose_conf_loss.pred_diff_angle, 2));
    obj_conf_diff(1:8, :) = pose_conf_loss.pred_diff_angle ./ size(pose_conf_loss.pred_diff_angle, 2);
    obj_conf_diff(9:16, :) = pose_conf_loss.pred_diff_vec1 ./ size(pose_conf_loss.pred_diff_vec1, 2);
    obj_conf_diff(17:24, :) = pose_conf_loss.pred_diff_vec2 ./ size(pose_conf_loss.pred_diff_vec2, 2);
    obj_conf_diff(25:32, :) = pose_conf_loss.pred_diff_vec3 ./ size(pose_conf_loss.pred_diff_vec3, 2);
    for i_im = 1 : net_opt.batch_size

        pos_dbox_idx = pos_dbox_all{i_im};
        if isempty(pos_dbox_idx)
           continue;
        end

        conf_diff(net_input_label{i_im}(:, 5), :, pos_dbox_idx, i_im) = obj_conf_diff(:, 1:numel(pos_dbox_idx));
        obj_conf_diff(:, 1:numel(pos_dbox_idx)) = [];
    end
    conf_diff = reshape(conf_diff, [], net_opt.batch_size);
    caffe_solver.net.blobs('mbox_pose_conf').set_diff(conf_diff);

end
