function R = Angle2RotMat(azimuth, elevation)
    azimuth = azimuth*pi/180;
    elevation = elevation*pi/180;
    % Rotate coordinate system by theta is equal to rotating the model by -theta.
    azimuth = -azimuth;
    elevation = -(pi/2-elevation);

    % rotation matrix
    Rz = [cos(azimuth) -sin(azimuth) 0; sin(azimuth) cos(azimuth) 0; 0 0 1];   %rotate by a
    Rx = [1 0 0; 0 cos(elevation) -sin(elevation); 0 sin(elevation) cos(elevation)];   %rotate by e
    R = Rx*Rz;
end