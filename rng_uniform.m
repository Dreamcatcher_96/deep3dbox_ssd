function rngNums = rng_uniform(n, a, b)
    persistent tseed;
    assert(a <= b, 'a must be <= b');
    
    if isempty(tseed)
       tseed = get_timestamp(); 
       rng(tseed);
    end
    
    rngNums = rand(1, n) * abs(b - a) + a;
end