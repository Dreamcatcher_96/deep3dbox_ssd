function [class_ap, class_pose_acc] = ResEvaluate(loc_pred, conf_pred, pose_loc_pred, pose_conf_pred, ...
                                                    img, obj_bbox, obj_class, obj_quat, obj_diff, all_class_name, net_opt)
    
    num_class = numel(all_class_name);
    bbox_pred_batch = cell(num_class, length(img));
    score_pred_batch = cell(num_class, length(img));
    pose_pred_batch = cell(num_class, length(img));
    
    aboxes = cell(num_class, net_opt.batch_size);
    ground_truth = cell(num_class, net_opt.batch_size);
    pose_gt = cell(num_class, net_opt.batch_size);
    obj_difficulty = cell(num_class, net_opt.batch_size);
    for i_img = 1 : length(img)
        loc_pred_now = loc_pred(:, :, i_img);
        conf_pred_now = conf_pred(:, :, i_img);
        pose_conf_pred_now = pose_conf_pred(:, :, :, i_img);
        pose_loc_pred_now = pose_loc_pred(:, :, :, i_img);
        [bbox_pred_batch(:, i_img), score_pred_batch(:, i_img), pose_pred_batch(:, i_img)] = ...
            DetectionInference(img{i_img}, net_opt.dbox, loc_pred_now, ...
                                                        conf_pred_now, pose_conf_pred_now, pose_loc_pred_now, net_opt);
    end

    % save detection results
    for i_class = 1 : num_class
        aboxes(i_class, :) = cellfun(@(x,y,z) [x', y', z], ...
            bbox_pred_batch(i_class, :), score_pred_batch(i_class, :), pose_pred_batch(i_class, :), 'UniformOutput', false);
        this_class = cellfun(@(x) x == i_class, ...
                    obj_class, 'UniformOutput', false);
        ground_truth(i_class, :) = ...
            cellfun(@(x,y) x(:, y)', obj_bbox, this_class, 'UniformOutput', false);            
        pose_gt(i_class, :) = ...
            cellfun(@(x,y) x(:, y)', obj_quat, this_class, 'UniformOutput', false);
        obj_difficulty(i_class, :) = cellfun(@(x,y) x(y), ...
                    obj_diff, this_class, 'UniformOutput', false);
    end
    [class_ap, class_pose_acc, ~, ~] = DetectionResEval(aboxes, ground_truth, pose_gt, ...
            obj_difficulty, all_class_name, all_class_name, net_opt.nms_overlap);
                                                
end